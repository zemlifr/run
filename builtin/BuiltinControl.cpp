#include "BuiltinControl.h"

BuiltinQuit::BuiltinQuit(VirtualMachine* vm) : vm(vm)
{
	expectedArgs = 0;
}

CObject* BuiltinQuit::exec(CList* args)
{
	vm->stop();
	return NIL;
}

CObject* BuiltinHelp::exec(CList* args)
{
	for (auto it = args->getIterator(); it.hasNext();++it)
	{
		CObject * item = it.item();
		std::cout << *item;
	}

	return NIL;
}