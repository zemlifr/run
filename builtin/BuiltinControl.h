#pragma once

#include "../lang/CFunction.h"
#include "../runtime/VirtualMachine.h"

class BuiltinQuit :
	public BuiltinFunction
{
public:
	BuiltinQuit(VirtualMachine *vm);

protected:
	CObject* exec(CList* args) override;

private:
	VirtualMachine *vm;
};

class BuiltinHelp :
	public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;
};