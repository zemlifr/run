#include  "BuiltinList.h"

#include "../lang/Clist.h"
#include "../lang/ISequence.h"
#include "../memory/Memory.h"
#include "../util/Exceptions.h"

CObject* BuiltinList::exec(CList* args)
{
  return args;
}

BuiltinSize::BuiltinSize()
{
	expectedArgs = 1;
}

CObject * BuiltinSize::exec(CList * args)
{
	if (ISequence *seq = dynamic_cast<ISequence *>(args->first()))
		return NEWLONG(seq->size());

	return NEWLONG(0);
}

BuiltinFirst::BuiltinFirst()
{
	expectedArgs = 1;
}

CObject* BuiltinFirst::exec(CList* args)
{
	if (ISequence *seq = dynamic_cast<ISequence *>(args->first()))
		return seq->first();

	throw RuntimeError("Invalid argument type. Expected LIST.");
}

BuiltinLast::BuiltinLast()
{
	expectedArgs = 1;
}

CObject* BuiltinLast::exec(CList* args)
{
	if (ISequence *seq = dynamic_cast<ISequence *>(args->first()))
		return seq->last();

	throw RuntimeError("Invalid argument type. Expected LIST.");
}

BuiltinRest::BuiltinRest()
{
	expectedArgs = 1;
}

CObject* BuiltinRest::exec(CList* args)
{
	if(ISequence *seq = dynamic_cast<ISequence *>(args->first()))
		return seq->rest();
	
	throw RuntimeError("Invalid argument type. Expected LIST.");
}

BuiltinInsertFront::BuiltinInsertFront()
{
	expectedArgs = 2;
}

CObject* BuiltinInsertFront::exec(CList* args)
{
	if (ISequence * first = dynamic_cast<ISequence *>(args->at(0)))
	{
		CObject * obj = args->at(1);

		first->insertFront(obj);

		return first;
	}
	return NIL;
}

BuiltinInsertBack::BuiltinInsertBack()
{
	expectedArgs = 2;
}

CObject* BuiltinInsertBack::exec(CList* args)
{
	if (ISequence * first = dynamic_cast<ISequence *>(args->at(0)))
	{
		CObject * obj = args->at(1);
		first->insertBack(obj);

		return first;
	}
	return NIL;
}

BuiltinSeq::BuiltinSeq()
{
	expectedArgs = 3;
}

CObject* BuiltinSeq::exec(CList* args)
{
	if (args->size() == 2)
		return (CObject *)NEWSEQUENCE(((CLong *)args->at(0)), (CLong *)args->at(2),NEWLONG(1));
	if(args->size() == 3)
		return (CObject *)NEWSEQUENCE(((CLong *)args->at(0)), (CLong *)args->at(1), (CLong *)args->at(2));

	return NIL;
}

BuiltinClone::BuiltinClone()
{
	expectedArgs = 1;
}

CObject* BuiltinClone::exec(CList* args)
{
	return args->first()->clone();
}

CObject* BuiltinMap::exec(CList* args)
{
	CMap * map = NEWMAP;
	while(true)
	{
		CObject * key = args->first();
		CObject * val = args->rest()->first();
		if (key == NIL || val == NIL)
			break;

		map->put(key, val);
		args = args->rest()->rest();
	}
	return map;
}

CObject* BuiltinConcat::exec(CList* args)
{
	if (args->size() == 0)
		return NIL;
	if (args->size() == 1)
		return args->first();

	ISequence *res = dynamic_cast<ISequence *>(args->first());
	if (res)
	{
		//todo use iterator
		for (int i = 1; i < args->size(); i++)
		{
			ISequence * seq = dynamic_cast<ISequence *>(args->at(i));
			if (seq)
				res->concat(seq);
			else
				return NIL;//todo exception
		}
	}
	return res;
}

BuiltinAt::BuiltinAt()
{
	expectedArgs = 2;
}

CObject* BuiltinAt::exec(CList* args)
{
	CObject * obj = args->at(0);
	CObject * index = args->at(1);

	//dynamic cast, so it can be applied on every future ISequence subclass
	if(ISequence *s = dynamic_cast<ISequence*>(obj))
	{
		if (index->getType() == LONG)
			return s->at(static_cast<CLong *>(index)->getVal());
	}

	if (CMap *m = dynamic_cast<CMap*>(obj))
	{
			return m->get(index);
	}

	return NIL;
}

BuiltinSetf::BuiltinSetf()
{
	expectedArgs = 3;
}

CObject* BuiltinSetf::exec(CList* args)
{
	CObject * obj = args->at(0);
	CObject * index = args->at(1);
	CObject * item = args->at(2);


	//dynamic cast, so it can be applied on every future ISequence subclass
	if (ISequence *s = dynamic_cast<ISequence*>(obj))
	{
		if (index->getType() == LONG)
			return s->setf(static_cast<CLong *>(index)->getVal(),item);
	}

	if (CMap *m = dynamic_cast<CMap*>(obj))
	{
		m->put(index,item);
		return m;
	}

	return NIL;
}