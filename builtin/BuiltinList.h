#pragma once
#include "BuiltinMath.h"

class BuiltinList
	: public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;
};

class BuiltinSize
	: public BuiltinFunction
{
public:
	BuiltinSize();
protected:
	CObject* exec(CList* args) override;
};

class BuiltinFirst
	: public BuiltinFunction
{
public:
	BuiltinFirst();
protected:
	CObject* exec(CList* args) override;
};

class BuiltinLast
	: public BuiltinFunction
{
public:
	BuiltinLast();
protected:
	CObject* exec(CList* args) override;
};

class BuiltinRest
	: public BuiltinFunction
{
public:
	BuiltinRest();
protected:
	CObject* exec(CList* args) override;
};

class BuiltinInsertFront
	: public BuiltinFunction
{
public:
	BuiltinInsertFront();
protected:
	CObject* exec(CList* args) override;
};

class BuiltinInsertBack
	: public BuiltinFunction
{
public:
	BuiltinInsertBack();
protected:
	CObject* exec(CList* args) override;
};

class BuiltinSeq
	: public BuiltinFunction
{
public:
	BuiltinSeq();
protected:
	CObject* exec(CList* args) override;
};

class BuiltinClone
	: public BuiltinFunction
{
public:
	BuiltinClone();
protected:
	CObject* exec(CList* args) override;
};

class BuiltinMap
	: public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;
};

class BuiltinConcat
	 : public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;
};

class BuiltinAt
	: public BuiltinFunction
{
public:
	BuiltinAt();

protected:
	CObject* exec(CList* args) override;
};

class BuiltinSetf
	: public BuiltinFunction
{
public:
	BuiltinSetf();
protected:
	CObject * exec(CList* args) override;
};



