#include "BuiltinMath.h"
#include "../lang/Clist.h"
#include "../memory/Memory.h"
#include "../util/Utils.h"

CObject* BuiltinPlus::exec(CList* args)
{
	CList * list = static_cast<CList *>(args);
	long long result = 0;

	for (auto it = list->getIterator(); it.hasNext(); ++it)
	{
		if (it.item()->getType() == LONG)
			result += (static_cast<CLong*>(it.item()))->getVal();
		else if (it.item()->getType() == FLOAT)
			return floatPlus(result, it);
	}

	return NEWLONG(result);
}

CFloat* BuiltinPlus::floatPlus(double val, CListIterator & it)
{
	double result = val;

	for (; it.hasNext(); ++it)
	{
		if (it.item()->getType() == LONG)
			result += (static_cast<CLong*>(it.item()))->getVal();
		else if (it.item()->getType() == FLOAT)
			result += (static_cast<CFloat*>(it.item()))->getVal();
	}

	return NEWFLOAT(result);
}

CObject* BuiltinMinus::exec(CList* args)
{
	if (args->size() < 1)
		return NIL;
  auto it = args->getIterator();
  ++it;

  if (args->at(0)->getType() == FLOAT)
  {
	  return floatMinus(((CFloat*)args->at(0))->getVal(), it);
  }
  else if (args->at(0)->getType() == LONG)
  {
	  long long result = (static_cast<CLong*>(args->getIterator().item()))->getVal();

	  for (; it.hasNext(); ++it)
	  {
		  if (it.item()->getType() == LONG)
			  result -= (static_cast<CLong*>(it.item()))->getVal();
		  else if (it.item()->getType() == FLOAT)
			  return floatMinus(result, it);
	  }

	  return NEWLONG(result);
  }

  return NIL; //TODO: exception on wrong args
}

CObject * BuiltinMinus::floatMinus(double val, CListIterator it)
{
  double result = val;

  for (; it.hasNext(); ++it)
  {
    if (it.item()->getType() == LONG)
      result -= (static_cast<CLong*>(it.item()))->getVal();
    else if (it.item()->getType() == FLOAT)
      result -= (static_cast<CFloat*>(it.item()))->getVal();
  }

  return NEWFLOAT(result);
}


CObject* BuiltinTimes::exec(CList* args)
{
	CList * list = static_cast<CList *>(args);
	long long result = 1;

	for (auto it = list->getIterator(); it.hasNext(); ++it)
	{
		if (it.item()->getType() == LONG)
			result *= (static_cast<CLong*>(it.item()))->getVal();
		else if (it.item()->getType() == FLOAT)
			return floatTimes(result, it);
	}

	return NEWLONG(result);
}

CObject* BuiltinTimes::floatTimes(double val, CListIterator it)
{
	double result = val;

	for (; it.hasNext(); ++it)
	{
		if (it.item()->getType() == LONG)
			result *= (static_cast<CLong*>(it.item()))->getVal();
		else if (it.item()->getType() == FLOAT)
			result *= (static_cast<CFloat*>(it.item()))->getVal();
	}

	return NEWFLOAT(result);
}

CObject* BuiltinDivide::exec(CList* args)
{
	if (args->size() < 1)
		return NIL;

	BuiltinTimes tm;
	CObject * result = tm(args->rest());

	double retval;

	if (CLong *lng = dynamic_cast<CLong*>(args->first()))
		retval = lng->getVal();
	else if (CFloat *fl = dynamic_cast<CFloat*>(args->first()))
		retval = fl->getVal();

	if (result->isTrue()) //not zero or nil
	{
		if (CLong *lng = dynamic_cast<CLong*>(result))
			return NEWFLOAT(retval / lng->getVal());
		else if (CFloat *fl = dynamic_cast<CFloat*>(result))
			return NEWFLOAT(retval / fl->getVal());
	}
	
   return NIL;
}

CObject* BuiltinDivide::floatDivide(double val, CListIterator it)
{
  double result = val;

  for (; it.hasNext(); ++it)
  {
    if (it.item()->getType() == LONG)
      result /= (static_cast<CLong*>(it.item()))->getVal();
    else if (it.item()->getType() == FLOAT)
      result /= (static_cast<CFloat*>(it.item()))->getVal();
  }

  return NEWFLOAT(result);
}

BuiltinBaseEquals::BuiltinBaseEquals()
{
	expectedArgs = 2;
}

CObject* BuiltinBaseEquals::exec(CList* args)
{
	ObjectComparator cmp;

	return cmp(args->at(0), args->at(1)) ? TRUE : FALSE;
}

CObject* BuiltinGreaterThan::exec(CList* args)
{
  CList * list = static_cast<CList *>(args);
  bool result = false;
  long long val = 0;
  long long other = 0;
  CListIterator iterator = list->getIterator();
  if (iterator.hasNext())
  {
    val = (static_cast<CLong*>(iterator.item()))->getVal();
  }
  else
  {
    //TODO throw wrong argument count exception
  }
  ++iterator;
  if (iterator.hasNext())
  {
    other = (static_cast<CLong*>(iterator.item()))->getVal();
  }
  else
  {
    //TODO throw wrong argument count exception
  }
  if (val > other)
    result = true;
  return result ? TRUE : FALSE;
}

CObject* BuiltinSmallerThan::exec(CList* args)
{
  CList * list = static_cast<CList *>(args);
  bool result = false;
  long long val = 0;
  long long other = 0;
  CListIterator iterator = list->getIterator();
  if (iterator.hasNext())
  {
    val = (static_cast<CLong*>(iterator.item()))->getVal();
  }
  else
  {
    //TODO throw wrong argument count exception
  }
  ++iterator;
  if (iterator.hasNext())
  {
    other = (static_cast<CLong*>(iterator.item()))->getVal();
  }
  else
  {
    //TODO throw wrong argument count exception
  }
  if (val < other)
    result = true;
  return result ? TRUE : FALSE;
}

CObject * BuiltinAnd::exec(CList * args)
{
	for (auto it = args->getIterator(); it.hasNext();++it)
	{
		if (!it.item()->isTrue())
			return FALSE;
	}

	return TRUE;
}

CObject * BuiltinOr::exec(CList * args)
{

	for (auto it = args->getIterator(); it.hasNext();++it)
	{
		if (it.item()->isTrue())
			return TRUE;
	}

	return FALSE;
}

BuiltinNot::BuiltinNot()
{
	expectedArgs = 1;
}

CObject * BuiltinNot::exec(CList * args)
{
	if (args->at(0)->isTrue())
		return FALSE;
	return TRUE;
}
