#pragma once
#include "../lang/CFunction.h"
#include "../lang/Atoms.h"

class CListIterator;

class BuiltinPlus
	: public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;

private:
	CFloat* floatPlus(double val, CListIterator & it);
};

class BuiltinMinus 
	: public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;
private:
  CObject* floatMinus(double val, CListIterator it);
};

class BuiltinTimes
	: public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;
private:
	CObject* floatTimes(double val, CListIterator it);
};

class BuiltinDivide
  : public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
private:
  CObject* floatDivide(double val, CListIterator it);
};

class BuiltinBaseEquals
  : public BuiltinFunction
{
public:
	BuiltinBaseEquals();
protected:
  CObject* exec(CList* args) override;
};

class BuiltinGreaterThan
  : public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
};

class BuiltinSmallerThan
  : public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
};


class BuiltinAnd
	: public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;
};

class BuiltinOr
	: public BuiltinFunction
{
protected:
	CObject* exec(CList* args) override;
};

class BuiltinNot
	: public BuiltinFunction
{
public:
	BuiltinNot();
protected:
	CObject* exec(CList* args) override;
};