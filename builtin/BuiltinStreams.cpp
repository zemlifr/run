#include "BuiltinStreams.h"
#include "../lang/Clist.h"
#include "../lang/Atoms.h"
#include "../lang/CStream.h"
#include "../memory/Memory.h"
#include "../parser/Parser.h"
#include "../parser/Lexer.h"
#include <iostream>



BuiltinLoad::BuiltinLoad(VirtualMachine * vm)
{
  this->vm = vm;
}

CObject* BuiltinLoad::exec(CList* args)
{
  CString* path = static_cast<CString *>(args->first());
  std::filebuf fb;
  if (fb.open(path->getVal(), std::ios::in))
  {
    std::istream is(&fb);
    Lexer lexer(is);
    Parser parser(lexer);
    while (true)
    {
      CObject* exp = parser.parse();
      if (exp != nullptr)
      {
        std::cout << "file-input> ";
        exp->print(std::cout);
        std::cout << std::endl;
        CObject *result = exp->eval(vm);
        std::cout << *result << std::endl;
      }
      parser.getLexer().readNext();
      if (parser.getLexer().getToken().getType() == Token::END)
      {
        break;
      }
    }

    fb.close();
  }
  return NIL;
}

CObject* BuiltinPrint::exec(CList* args)
{
  if (args->first()->getType() != STREAM) //printing to std stream
  {
    for (auto it = args->getIterator(); it.hasNext(); ++it)
    {
      STDOUT->write(it.item());
    }
  }
  else
  {
    CStream* stream = static_cast<CStream *>(args->first());
    CList* toPrint = args->rest();
    for (auto it = toPrint->getIterator(); it.hasNext(); ++it)
    {
      stream->write(it.item());
    }
  }
  return NIL;
}

CObject* BuiltinOpen::exec(CList* args)
{
  CString* path = static_cast<CString *>(args->first());
  bool append = true;
  if (args->size() > 1)
  {
    append = static_cast<CBool *>(args->rest()->first())->isTrue();
  }
  CStream* stream = NEWSTREAM(path->getVal(), append);
  return stream;
}

CObject* BuiltinRead::exec(CList* args)
{
  FStream* stream = static_cast<FStream *>(args->first());
  return stream->read();
}

CObject* BuiltinReadLine::exec(CList* args)
{
  FStream* stream = static_cast<FStream *>(args->first());
  return stream->readLine();
}

CObject* BuiltinReadObject::exec(CList* args)
{
  FStream* stream = static_cast<FStream *>(args->first());
  Lexer lexer(stream->fs);
  Parser parser(lexer);
  CObject* parsed = parser.parse();
  return parsed;
}

CObject* BuiltinClose::exec(CList* args)
{
  CStream* stream = static_cast<CStream *>(args->first());
  stream->close();
  return NIL;
}