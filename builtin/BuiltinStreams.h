#pragma once
#include "../lang/CFunction.h"
#include "../runtime/VirtualMachine.h"

class BuiltinLoad :
  public BuiltinFunction
{
public:
  BuiltinLoad(VirtualMachine* vm);
protected:
  CObject* exec(CList* args) override;
private:
  VirtualMachine *vm;
};

class BuiltinPrint :
  public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
};

class BuiltinOpen:
  public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
};

class BuiltinRead :
  public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
};

class BuiltinReadLine :
  public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
};

class BuiltinReadObject :
  public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
};

class BuiltinClose :
  public BuiltinFunction
{
protected:
  CObject* exec(CList* args) override;
};

