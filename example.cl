(defn logger (filename) ((lambda (str)(lambda (msg)(print str msg)(print str "\n")))(open filename true)))

(defmacro FOR (what from start to end do body) `((lambda (,what max) 
                                              (if (not(eq ,what max))
                                                (do @,body ((this) (+ 1 ,what) max))
                                               )
                                            ) ,start ,end  )
)