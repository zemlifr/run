#include "Reader.h"
#include "../parser/Parser.h"
#include "../util/Exceptions.h"
#include "../memory/Memory.h"


Reader::Reader()
{
}


Reader::~Reader()
{
}

CObject* Reader::read()
{
	CObject *obj = NIL;

	try
	{
		Lexer lexer;
		Parser parser(lexer);
		obj = parser.parse();
	}
	catch(ParseError e)
	{
		std::cout << e.what();
	}
	
	return obj;
}