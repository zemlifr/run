#pragma once
#include "../lang/CObject.h"
#include "../parser/Lexer.h"

class Reader
{
public:
	Reader();
	~Reader();

	CObject * read();
	
};

