#include "Atoms.h"
#include "../runtime/IEvaluator.h"
#include "../memory/Memory.h"
#include <sstream>

CObject* CNil::eval(IEvaluator* e)
{
	return this;
}

void CNil::mark()
{
  marked = true;
}

void CNil::unmark()
{
  marked = false;
}

void CNil::print(std::ostream& where) const
{
	//where << "nil";
}

Type CNil::getType() const
{
	return TNIL;
}

bool CNil::isTrue()
{
	return false;
}

CString::CString(){}

CString::CString(const std::string & str) : value(str){}

CObject* CString::eval(IEvaluator* e)
{
	return this;
}

CObject* CString::clone()
{
	return NEWSTRING(value);
}

void CString::mark()
{
  marked = true;
}

void CString::unmark()
{
  marked = false;
}

CString::~CString(){}

std::string CString::getVal() const
{
	return value;
}

Type CString::getType() const
{
	return STRING;
}

void CString::print(std::ostream& where) const
{
	//where << "\"";
	where << value;
	//where << "\"";
}

bool CString::isTrue()
{
	return !value.empty();
}

int CString::size() const
{
	return value.size();
}

CObject * CString::at(int idx)
{
	return NEWSTRING(std::string(1, value[idx]));
}

CObject* CString::first()
{
	return NEWSTRING(std::string(1,value[0]));
}

CString* CString::rest()
{
	return NEWSTRING(value.substr(1));
}

CObject* CString::last()
{
	return NEWSTRING(std::string(1, value.back()));
}

void CString::insertBack(CObject* exp)
{
	std::stringstream ss;
	ss << value << *exp;
	value = ss.str();
}

void CString::insertFront(CObject* exp)
{
	std::stringstream ss;
	ss << *exp << value;
	value = ss.str();
}

ISequence* CString::concat(ISequence* seq)
{
	std::stringstream ss;
	ss << value << *seq;
	value = ss.str();

	return this;
}

ISequence* CString::setf(int idx, CObject* val)
{
	return this; //todo implement
}

CSymbol::CSymbol(){}

CSymbol::CSymbol(const std::string& str) : value(str){}

CObject* CSymbol::eval(IEvaluator* e)
{
	return e->eval(this);
}

void CSymbol::mark()
{
  marked = true;
}

void CSymbol::unmark()
{
  marked = false;
}

CSymbol::~CSymbol(){}

Type CSymbol::getType() const
{
	return SYMBOL;
}

std::string CSymbol::getVal() const
{
	return value;
}

void CSymbol::print(std::ostream& where) const
{
	where << value;
}

CFloat::CFloat() : value(0){}

CFloat::CFloat(double val) : value(val){}

CObject* CFloat::eval(IEvaluator* e)
{
	return this;
}

void CFloat::mark()
{
  marked = true;
}

void CFloat::unmark()
{
  marked = false;
}

CFloat::~CFloat(){}

double CFloat::getVal() const
{
	return value;
}

Type CFloat::getType() const
{
	return FLOAT;
}

void CFloat::setVal(double val)
{
	this->value = val;
}

void CFloat::print(std::ostream& where) const
{
	where << value;
}

bool CFloat::isTrue()
{
	return value;
}

CLong::CLong() : value(0){}

CLong::CLong(long long value) : value(value){}

CObject* CLong::eval(IEvaluator* e)
{
	return this;
}

void CLong::mark()
{
  marked = true;
}

void CLong::unmark()
{
  marked = false;
}

CLong::~CLong()
{
}

long long CLong::getVal() const
{
	return value;
}

Type CLong::getType() const
{
	return LONG;
}

void CLong::setVal(long long val)
{
	this->value = val;
}

void CLong::print(std::ostream& where) const
{
	where << value;
}

bool CLong::isTrue()
{
	return value;
}

CBool::CBool() : value(false){}

CBool::CBool(bool value) : value(value) {}

Type CBool::getType() const
{
	return BOOL;
}

CBool::~CBool(){}

CObject* CBool::eval(IEvaluator* e)
{
	return this;
}

void CBool::mark()
{
  marked = true;
}

void CBool::unmark()
{
  marked = false;
}

void CBool::print(std::ostream& where) const
{
	where << value;
}

bool CBool::isTrue()
{
	return value;
}