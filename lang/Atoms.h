#pragma once
#include "CObject.h"
#include "ISequence.h"

#include <string>

class CNil :
	public CObject
{
public:
	CObject* eval(IEvaluator* e) override;
	void mark() override;
  void unmark() override;
	void print(std::ostream& where) const override;
	Type getType() const override;
	virtual bool isTrue() override;
};

class CString :
	public ISequence
{
public:
	CString();
	CString(const std::string & str);
	virtual ~CString();

	std::string getVal() const;

	Type getType() const override;
	CObject* eval(IEvaluator* e) override;
	CObject* clone() override;
	void mark() override;
  void unmark() override;
	virtual void print(std::ostream& where) const override;
	virtual bool isTrue() override;

	//ISequence interface
	int size() const override;
	CObject * at(int idx) override;
	CObject* first() override;
	CString* rest() override;
	CObject* last() override;
	void insertBack(CObject* exp) override;
	void insertFront(CObject* exp) override;
	ISequence* concat(ISequence* seq) override;
	ISequence* setf(int idx, CObject* val) override;
protected: 
	std::string value;
};

class CSymbol :
	public CObject
{
public:
	CSymbol();
	CSymbol(const std::string & str);
	virtual ~CSymbol();
	std::string getVal() const;

	virtual Type getType() const override;
	CObject* eval(IEvaluator* e) override;
	void mark() override;
  void unmark() override;
	virtual void print(std::ostream& where) const override;
private:
	std::string value;
};

class CFloat :
	public CObject
{
public:
	CFloat();
	CFloat(double val);
	virtual ~CFloat();

	double getVal() const;
	void setVal(double val);


	Type getType() const override;
	CObject* eval(IEvaluator* e) override;
	void mark() override;
  void unmark() override;
	virtual void print(std::ostream& where) const override;
	virtual bool isTrue() override;

private:
	double value;
};

class CLong :
	public CObject
{
public:
	CLong();
	CLong(long long value);
	virtual ~CLong();

	long long getVal() const;
	void setVal(long long val);


	Type getType() const override;
	CObject* eval(IEvaluator* e) override;
	void mark() override;
  void unmark() override;
	virtual void print(std::ostream& where) const override;
	virtual bool isTrue() override;
private:
	long long value;
};

class CBool :
	public CObject
{
public:
	CBool();
	CBool(bool value);
	~CBool();


	Type getType() const override;
	CObject* eval(IEvaluator* e) override;
	void mark() override;
  void unmark() override;
	void print(std::ostream& where) const override;
	virtual bool isTrue() override;
private:
	bool value;
};

