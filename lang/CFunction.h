#pragma once

#include <limits.h>
#include "CObject.h"
#include "Clist.h"
#include "../memory/Memory.h"

class CFunction
	: public CObject
{
public:
	CFunction(int argsCount = 0) : expectedArgs(argsCount)
	{
		memory.registerObject(this);
	}
	virtual ~CFunction() {};
	int expectedArgsCount(){return expectedArgs;}

protected:
	int expectedArgs;
};

/**
  *Abstract base for all builtin functions.
  *Builtin functions take 'infinite' arguments by default.
  */
class BuiltinFunction 
	: public CFunction
{
public:
	BuiltinFunction() : CFunction(INT_MAX) {}
	CObject* eval(IEvaluator* e) override
	{
		return this;
	}


	CObject* clone() override
	{
		return this;
	}

	void mark() override
   {
		marked = true;
   }

  void unmark() override
  {
    marked = false;
  }

	void print(std::ostream& where) const override
	{
		where << "<built-in function>";
	}

	CObject * operator() (CList*args)
	{
		return exec(args);
	}

	Type getType() const override
	{
		return BUILTIN_FUNCTION;
	}
protected:
	virtual CObject * exec(CList* args) = 0;
};