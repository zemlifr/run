#include "CMap.h"

#include "../memory/Memory.h"

CMap::CMap()
{
}


CMap::~CMap()
{
}

CObject * CMap::eval(IEvaluator * e)
{
	return this;
}

void CMap::mark()
{
  marked = true;
  for (auto it = data.begin(); it != data.end(); ++it)
  {
    it->first->mark();
    it->second->mark();
  }
}

void CMap::unmark()
{
  marked = true;
  for (auto it = data.begin(); it != data.end(); ++it)
  {
    it->first->unmark();
    it->second->unmark();
  }
}

void CMap::print(std::ostream & where) const
{
	where << "{";
	for (auto it = data.begin(); it != data.end(); ++it)
	{
		where << *(it->first);
		where << "=>";
		where << *(it->second);
		where << " ";
	}
	where << "}";
}

Type CMap::getType() const
{
	return MAP;
}

CObject * CMap::get(CObject * key)
{
	try
	{
		CObject *ret = data.at(key);
		return ret;
	}
	catch(std::out_of_range)
	{
		return NIL;
	}
	
}

void CMap::put(CObject * key, CObject * val)
{
	data[key] = val;
}

int CMap::size() const
{
	return data.size();
}

void CMap::remove(CObject * key)
{
	data.erase(key);
}
