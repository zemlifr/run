#pragma once
#include <unordered_map>
#include <functional>
#include <string>

#include "CObject.h"
#include "Atoms.h"
#include "../util/Utils.h"

class CMap :
	public CObject
{
public:
	CMap();
	virtual ~CMap();
	virtual CObject *eval(IEvaluator *e) override;
	virtual void mark() override;
  virtual void unmark() override;

	virtual void print(std::ostream &where) const override;
	virtual Type getType() const override;

	CObject * get(CObject *key);
	void put(CObject* key, CObject *val);
	int size() const;
	void remove(CObject *key);

private:

	std::unordered_map<CObject*, CObject*, AtomHash, ObjectComparator> data;
};



