#include "CObject.h"
#include "../runtime/IEvaluator.h"


CObject::CObject()
{
}


CObject::~CObject()
{
}

CObject* CObject::clone()
{
	return this;
}

bool CObject::isMarked()
{
  return marked;
}

Type CObject::getType() const
{
	return OBJECT;
}

bool CObject::isTrue()
{
	return true;
}

std::ostream& operator<<(std::ostream& out, const CObject& ex)
{
	ex.print(out);
	return out;
}