#pragma once
#include <iostream>

enum Type{OBJECT,TNIL,SYMBOL,LIST,LONG,FLOAT,STRING,BOOL,STREAM,MAP,
	BUILTIN_FUNCTION,USER_FUNCTION,SYNTAX,MACRO};

class IEvaluator;

class CObject
{
public:
	
	CObject();
	virtual ~CObject();
	virtual CObject * clone();
	virtual CObject *eval(IEvaluator *e) = 0;
    virtual void mark() = 0;
    virtual void unmark() = 0;
    virtual bool isMarked();

	virtual void print(std::ostream &where) const = 0;
	virtual Type getType() const;
	virtual bool isTrue();

	friend std::ostream& operator<< (std::ostream& out, const CObject& ex);

	protected:
	bool marked = false;
};
