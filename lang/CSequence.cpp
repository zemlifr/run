#include "CSequence.h"

#include "../memory/Memory.h"

CSequence::CSequence(CLong* start, CLong* end, CLong* step) : ISequence()
{
	build(start, end, step);
}

CSequence::~CSequence()
{
}

Type CSequence::getType() const
{
	return LIST;
}

CObject* CSequence::eval(IEvaluator* e)
{
	return this;
}

void CSequence::mark()
{
	marked = true;
	start->mark();
	end->mark();
	step->mark();
}

void CSequence::unmark()
{
  marked = false;
  start->unmark();
  end->unmark();
  step->unmark();
}

void CSequence::print(std::ostream& where) const
{
	where << *start << "..." << *end;
}

CObject* CSequence::at(int idx)
{
	if (idx < mSize)
		return op(idx);

	return NIL;
}

int CSequence::size() const
{
	return mSize;
}

CObject* CSequence::first()
{
	return start;
}

ISequence* CSequence::rest()
{
	if (abs(end->getVal() - start->getVal()) < step->getVal())
	{
		auto list = NEWLIST;
		//list->insertFront(start);
		return list;
	}
	return NEWSEQUENCE(op(1),end,step);
}

CObject* CSequence::last()
{
	return end;
}

void CSequence::insertBack(CObject* exp)
{//unsupported
}

void CSequence::insertFront(CObject* exp)
{//unsupported
}

ISequence* CSequence::concat(ISequence* seq)
{

	return this; //todo implement
}

ISequence* CSequence::setf(int idx, CObject* val)
{//unsupported
	return this;
}

ISequence::SequenceIterator* CSequence::iterator()
{
	return new CSequenceIterator(this);
}

void CSequence::build(CLong* start, CLong* end, CLong* step)
{
	this->start = start;
	this->end = end;
	this->step = step;
	auto s = start->getVal();
	auto e = end->getVal();
	auto st = step->getVal();
	if (st == 0)
		mSize = 0;
	else
		mSize = ((abs(e - s)) / st)+1;

	if (s<=e)
		op = [s, st](int idx) {return NEWLONG(s + st*idx); };
	else
		op = [s, st](int idx) {return NEWLONG(s - st*idx); };
}

CSequenceIterator::CSequenceIterator(CSequence* seq) : seq(seq), i(0){}

CSequenceIterator::~CSequenceIterator()
{
}

bool CSequenceIterator::hasNext() const
{
	return i < seq->size();
}

CObject* CSequenceIterator::item() const
{
	return seq->at(i);
}

void CSequenceIterator::operator++(){i++;}
