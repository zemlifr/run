#pragma once
#include  <functional>

#include "ISequence.h"
#include "Atoms.h"

class CSequence :
	public ISequence
{
public:
	CSequence(CLong* start, CLong* end, CLong* step);
	virtual ~CSequence();
	Type getType() const override;
	CObject* eval(IEvaluator* e) override;
	void mark() override;
  void unmark() override;
	void print(std::ostream& where) const override;
	CObject* at(int idx) override;
	int size() const override;
	CObject* first() override;
	ISequence* rest() override;
	CObject* last() override;
	void insertBack(CObject* exp) override;
	void insertFront(CObject* exp) override;
	ISequence* concat(ISequence* seq) override;
	ISequence* setf(int idx, CObject* val) override;
	SequenceIterator* iterator() override;

private:
	void build(CLong* start, CLong* end, CLong* step);

	int mSize;
	CLong *start;
	CLong* end;
	CLong* step;
	std::function<CLong*(int)> op;

};

class CSequenceIterator 
	: public ISequence::SequenceIterator
{
public:
	CSequenceIterator(CSequence * seq);
	~CSequenceIterator() override;
	bool hasNext() const override;
	CObject* item() const override;
	void operator++() override;

private:
	CSequence *seq;
	int i;
};

