#include "CStream.h"
#include "../memory/Memory.h"

CStream::CStream()
{
}

CStream::~CStream()
{
}

CObject * CStream::eval(IEvaluator * e)
{
	return this;
}

void CStream::print(std::ostream & where) const
{
	where << "<STREAM>";
}

void CStream::mark()
{
	marked = true;
}

void CStream::unmark()
{
  marked = false;
}

Type CStream::getType() const
{
  return STREAM;
}

FStream::FStream(CString * source, bool append)
{
  if (append)
  {
    fs.open(source->getVal(), std::fstream::in | std::fstream::out | std::ios::app);
  }
  else fs.open(source->getVal(), std::fstream::in | std::fstream::out | std::fstream::trunc);
	
}

FStream::FStream(std::string source, bool append)
{
  if (append)
  {
    fs.open(source, std::fstream::in | std::fstream::out | std::ios::app);
  }
  else fs.open(source, std::fstream::in | std::fstream::out | std::fstream::trunc);

}

FStream::~FStream()
{
	fs.close();
}

void FStream::write(CObject * obj)
{
  fs << *obj;
}

CString* FStream::read()
{
  char c;
  fs.get(c);
  std::string s(1, c);
  return NEWSTRING(s);
}

CString* FStream::readLine()
{
  std::string line;
  std::getline(fs, line);
  return NEWSTRING(line);
}

void FStream::close()
{
	fs.close();
}

StdOut::StdOut()
{
}

void StdOut::write(CObject * obj)
{
	obj->print(std::cout);
}

void StdOut::close()
{
	//TODO: error, cant close stdout
}

StdIn::StdIn()
{
}

void StdIn::write(CObject* obj)
{
}

void StdIn::close()
{
	//error, cant close stdin
}