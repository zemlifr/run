#pragma once
#include "CObject.h"

#include "Atoms.h"

#include <fstream>

class CStream :
	public CObject
{
public:
	CStream();
	virtual ~CStream();
	CObject* eval(IEvaluator* e) override;
	void print(std::ostream& where) const override;
	void mark() override;
  void unmark() override;
  Type getType() const override;

  virtual void write(CObject *obj)=0;
	virtual void close()=0;
};

class FStream :
	public CStream
{
public:
	FStream(CString *source, bool append = true);
    FStream(std::string source, bool append = true);
	~FStream();
	virtual void write(CObject *obj) override;
  CString* read();
  CString* readLine();
	virtual void close() override;
  std::fstream fs;
private:
	
};

class StdOut :
	public CStream
{
public:
	StdOut();
	virtual void write(CObject *obj) override;
	virtual void close() override;
	
};

class StdIn :
	public CStream
{
public:
	StdIn();
	void write(CObject* obj) override;
	void close() override;

};