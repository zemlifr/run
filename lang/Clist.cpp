#include "Clist.h"
#include "../runtime/IEvaluator.h"
#include "../memory/Memory.h"


CList::CList() : front(nullptr), back(nullptr), mSize(0), tmp(nullptr),lastIndex(-1)
{
}


CList::~CList()
{
}

void CList::insertBack(CObject* exp)
{
	CNode *newNode = NEWNODE;
	newNode->value = exp;
	//empty list
	if (!front)
		front = back = newNode;
	else
	{
		back->next = newNode;
		back = newNode;
	}
	mSize++;
}

void CList::insertFront(CObject* exp)
{
	CNode *newNode = NEWNODE;
	newNode->value = exp;
		//empty list
	if (!front)
		front = back = newNode;
	else
	{
		newNode->next = front;
		front = newNode;
	}
	mSize++;
}

ISequence* CList::concat(ISequence* seq)
{
	//same type, make it quick
	if(CList * ls= dynamic_cast<CList *>(seq))
	{
		if(ls->size()>0)
		{
			back->next = ls->front;
			back = ls->back;
			mSize += ls->size();
		}
	}
	else
		for (int i = 0; i < seq->size(); i++)
		{
			this->insertBack(seq->at(i));
		}
	return this;
}

ISequence* CList::setf(int idx, CObject* val)
{
	CNode * node = nodei(idx);

	if (node)
		node->value = val;

	return this;
}

ISequence::SequenceIterator* CList::iterator()
{
	return new CListIterator(this);
}

int CList::size() const
{
	return mSize;
}

bool CList::isEmpty()
{
	return front == nullptr;
}

bool CList::isPure(Type t)
{
	for (auto it = getIterator(); it.hasNext();++it)
	{
		if (it.item()->getType() != t)
			return false;
	}

	return true;
}

int CList::indexOf(CObject* o) const
{
	int i = 0;
	for (auto it = getIterator(); it.hasNext(); ++it)
	{
		if (it.item() == o)
			return i;

		i++;
	}

	//throw exception
	return -1;
}

CObject* CList::first()
{
	if (front)
		return front->value;
	return NIL;
}

Type CList::getType() const
{
	return LIST;
}

CList* CList::rest()
{
	CList *rest = NEWLIST;
	//DO a deep copy?
  if (front && front->next)
  {
    rest->front = front->next;
    rest->back = back;
    rest->mSize = mSize - 1;
  }
	

	return rest;
}

CObject* CList::last()
{
	if (back)
		return back->value;

	return NIL;
}

CListIterator CList::getIterator()
{
	return CListIterator(this);
}

CListIterator CList::getIterator() const
{
	return CListIterator(this);
}

CObject* CList::at(int idx)
{
	CNode * node = nodei(idx);

	if (node)
		return node->value;

	return NIL;
}

CObject* CList::eval(IEvaluator* e)
{
	return e->eval(this);
}

CObject* CList::clone()
{
	CList *lst = NEWLIST;
	if (!isEmpty())
	{
		lst->front = static_cast<CNode *>(front->clone());
		return lst;
	}
	lst->mSize = mSize;
	
	//ineffective, but there is probaby no better way right now.
	CNode * nd = lst->front;
	while(nd->next)
	{
		nd = nd->next;
	}
	lst->back = nd;

	return lst;
}

void CList::mark()
{
  if (!marked)
  {
    marked = true;
  }
  CNode *nextNode = front;
  while (nextNode)
  {
    nextNode->mark();
    nextNode = nextNode->next;
  }
}

void CList::unmark()
{
  marked = false;
  CNode *nextNode = front;
  while (nextNode)
  {
    nextNode->unmark();
    nextNode = nextNode->next;
  }
}

void CList::print(std::ostream& where) const
{
	CNode *it = front;

	where << "(";

	while(it!=nullptr)
	{
		it->value->print(where);
		where << " ";
		it = it->next;
	}

	where << ")";
}

CNode::CNode()
{
}

CNode::~CNode()
{
}

CObject* CNode::eval(IEvaluator* e)
{
  return this;
}

void CNode::mark()
{
  marked = true;
  value->mark();
}

void CNode::unmark()
{
  marked = false;
  value->unmark();
}

CObject* CNode::clone()
{
	CNode *node = NEWNODE;
	node->value = value->clone();
	if (next)
		node->next = static_cast<CNode *>(next->clone());

	return node;
}

void CNode::print(std::ostream& where) const
{
  value->print(where);
}

CNode* CList::nodei(int idx)
{
	CNode * node = front;
	int i = 0;
	if (idx > lastIndex && tmp)
	{
		node = tmp;
		i = lastIndex;
	}

	for (; i < idx; i++)
	{
		if (node)
		{
			node = node->next;
		}
	}
	if (i == idx)
	{
		tmp = node;
		lastIndex = idx;
		return node;
	}

	return nullptr;
}

CListIterator::CListIterator(CList* list) : next(list->front){}

CListIterator::CListIterator(const CList* list) : next(list->front)
{
}

bool CListIterator::hasNext() const
{
	return next != nullptr;
}

CObject* CListIterator::item() const
{
	if(next)
		return next->value;

	return nullptr;
}

void CListIterator::operator++()
{
	if (next)
		next = next->next;
}