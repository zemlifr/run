#pragma once
#include "CObject.h"
#include "ISequence.h"

class CListIterator;

class CNode : public CObject
{
public:
	CNode();
	virtual ~CNode();
	CObject* eval(IEvaluator* e) override;
	void mark() override;
  void unmark() override;
	CObject* clone() override;
	void print(std::ostream& where) const override;
	CNode *next = nullptr;
	CObject *value = nullptr;

};

class CList :
	 public ISequence
{
public:
	CList();
	virtual ~CList();
	bool isEmpty();
	bool isPure(Type t);
	int indexOf(CObject *o) const;
	CListIterator getIterator();
	CListIterator getIterator() const;

	//ISequence interface
	CObject* at(int idx) override;
	int size() const override;
	CObject * first() override;
	CList *rest() override; 
	CObject* last() override;
	void insertBack(CObject *exp) override;
	void insertFront(CObject *exp) override;
	ISequence* concat(ISequence* seq) override;
	ISequence* setf(int idx, CObject *val) override;
	SequenceIterator * iterator() override;

	Type getType() const override;
	CObject* eval(IEvaluator* e) override;
	CObject* clone() override;
	void mark() override;
  void unmark() override;
	virtual void print(std::ostream& where) const override;
private:


	CNode * nodei(int idx);
	//members

	CNode *front;
	CNode *back;
	int mSize;
	CNode *tmp;
	int lastIndex;

	friend class CListIterator;
};

class CListIterator : public ISequence::SequenceIterator
{
public:
	CListIterator(CList *list);
	CListIterator( CList const*list);
	bool hasNext() const override;
	CObject * item() const override;
	
	void operator++() override;

private:
	CNode * next;
};


