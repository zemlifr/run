#include "Continuation.h"



Continuation::Continuation()
{
}


Continuation::~Continuation()
{
}

void Continuation::setArgs(CObject* args)
{
	this->args = args;
}

CObject* Continuation::getArgs()
{
	return args;
}

void Continuation::setEnvironment(Environment* e)
{
	this->env = e;
}

Environment* Continuation::getEnvironment()
{
	return env;
}

CObject* Continuation::eval(IEvaluator* e)
{
	return this;
}

void Continuation::mark()
{
  if (!marked)
  {
    marked = true;
    args->mark();
  }
}

void Continuation::print(std::ostream& where) const
{
	where << "<Continuation>";
}