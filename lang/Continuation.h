#pragma once
#include "CObject.h"
#include "../runtime/Environment.h"


/**
 * Class holding actual state of computation.
 * Wraps all arguments for functions
 */
class Continuation :
	public CObject
{
public:
	Continuation();
	~Continuation();

	void setArgs(CObject *args);
	CObject * getArgs();
	void setEnvironment(Environment *e);
	Environment * getEnvironment();

	CObject* eval(IEvaluator* e) override;
    void mark() override;
	void print(std::ostream& where) const override;

private:
	Environment *env;
	CObject *args;
};

