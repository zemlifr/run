#pragma once
#include "CObject.h"

class CList;

class ISequence : 
	public CObject
{
public:
	class SequenceIterator;

	ISequence() :CObject(){};
	virtual ~ISequence() {};
	virtual CObject * at(int idx) = 0;
	virtual int size() const = 0;
	virtual CObject * first() = 0;
	virtual ISequence * rest() = 0;
	virtual ISequence * concat(ISequence * seq) = 0;
	virtual ISequence * setf(int idx, CObject *val) = 0;
	virtual CObject* last() = 0;
	virtual void insertBack(CObject *exp) = 0;
	virtual void insertFront(CObject *exp) = 0;
	virtual SequenceIterator * iterator() { return new SequenceIterator; };

	class SequenceIterator
	{
	public:
		virtual ~SequenceIterator(){}
		virtual bool hasNext() const { return false; };
		virtual CObject * item() const { return nullptr; };

		virtual void operator++(){};
	};
};