#include  "Macro.h"
#include "../runtime/IEvaluator.h"
#include "../memory/Memory.h"
#include "../runtime/Environment.h"
#include "UserFunction.h"

Macro::Macro(UserFunction *fn, IEvaluator * e) : func(fn), eval(e)
{
}

Macro::~Macro()
{
}

CObject* Macro::apply(CList* args, Environment* environment)
{
	return eval->eval(this,args);
}

void Macro::mark()
{
	marked = true;
	func->mark();
}

void Macro::unmark()
{
  marked = false;
  func->unmark();
}

Type Macro::getType() const
{
	return MACRO;
}

UserFunction* Macro::getFunc()
{
	return func;
}