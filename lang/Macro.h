#pragma once
#include "Syntax.h"

class Macro : 
	public Syntax
{
public:
	Macro(UserFunction *fn, IEvaluator *e);
	~Macro();
	CObject *apply(CList *args,Environment *env);
	void mark() override;
    void unmark() override;
	Type getType() const override;
	UserFunction* getFunc();
private:
	UserFunction *func;
	IEvaluator *eval;
};
