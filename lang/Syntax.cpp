#include "Syntax.h"
#include "../runtime/IEvaluator.h"
#include "../memory/Memory.h"
#include "../runtime/Compiler.h"
#include "UserFunction.h"
#include "Macro.h"

Syntax::Syntax()
{
	memory.registerObject(this);
}

Syntax::~Syntax()
{
}

void Syntax::print(std::ostream& where) const
{
	where << "<syntax>";
}

Type Syntax::getType() const
{
	return SYNTAX;
}

CObject* Syntax::eval(IEvaluator* e)
{
	return this;
}

void Syntax::mark()
{
  marked = true;
}

void Syntax::unmark()
{
  marked = false;
}

CObject* ByteCodeSyntax::clone()
{
	return this;
}


std::vector<Instruction> Lambda::getByteCode(CList* args, UserFunction *context, Environment* env)
{
	auto fn = Compiler::compileLambda((CList *)args->first(),args->rest(),context,env);
	return Compiler::compileItem(fn,context,env);
}

std::vector<Instruction> SetSyntax::getByteCode(CList* args, UserFunction *context, Environment* env)
{
	std::vector<Instruction> bc;
	for (auto it = args->getIterator(); it.hasNext();++it)
	{
		CSymbol *key = static_cast<CSymbol *>(it.item());
		if(it.hasNext())
		{
			++it;
			CObject *bind = it.item();
			auto res = Compiler::compileItem(bind, context, env);
			bc.insert(bc.end(), res.begin(), res.end());
			int idx = context->getLocalAdress(key);
			if (idx < 0)
			{
				idx = memory.symbolTable().getSymbolAddress(key);

				if (Compiler::isOuter(key, context))
				{
					context->setClosureMark(true);
					bc.push_back(Instruction(STORE_OUTER, idx));
				}
				else
					bc.push_back(Instruction(STORE, idx));
			}
			else
				bc.push_back(Instruction(STORE_LOCAL, idx));
		}
		//else exception
	}

  return bc;
}


std::vector<Instruction> IfSyntax::getByteCode(CList* args, UserFunction *context, Environment* env)
{
  CObject * cond = args->first();
  CList * body = args->rest();
		
  std::vector<Instruction> bc;
  bc = Compiler::compileItem(cond, context, env);
  if (body->rest()->isEmpty())//no else
  {
    auto res = Compiler::compileItem(body->first(), context, env);
	bc.push_back(Instruction(JUMP_FALSE, res.size()+1)); //jump relative forward
    bc.insert(bc.end(), res.begin(), res.end());
  }
  else
  {
    //first do true statement
    auto res = Compiler::compileItem(body->first(), context, env);
	bc.push_back(Instruction(JUMP_FALSE, res.size() + 2)); //jump relative forward

    bc.insert(bc.end(), res.begin(), res.end());
                                        //then else statement
    res = Compiler::compileItem(body->rest()->first(), context, env);
	bc.push_back(Instruction(JUMP, res.size()+1)); // jump to end
    bc.insert(bc.end(), res.begin(), res.end());

  }

  return bc;
}

std::vector<Instruction> QuoteSyntax::getByteCode(CList* args, UserFunction *context, Environment* env)
{
	std::vector<Instruction> bc;

	for (auto it = args->getIterator(); it.hasNext(); ++it)
	{
		CObject *item = it.item();
		if (item->getType() == SYMBOL)
			bc.push_back(Instruction(PUSHS, memory.symbolTable().getSymbolAddress(static_cast<CSymbol*>(item))));
		else
			bc.push_back(Instruction(PUSHC, context->constTable().putConstant(item)));
	}

	return bc;
}

std::vector<Instruction> DoSyntax::getByteCode(CList * args, UserFunction *context, Environment * env)
{
	std::vector<Instruction> bc;
	for (auto it = args->getIterator(); it.hasNext(); ++it)
	{
		auto res = Compiler::compileItem(it.item(), context, env);
		bc.insert(bc.end(), res.begin(), res.end());
	}

	return bc;
}

std::vector<Instruction> DefnSyntax::getByteCode(CList * args, UserFunction * context, Environment * env)
{
	CSymbol * name = static_cast<CSymbol *>(args->at(0));
	CList * fnArgs = static_cast<CList*>(args->at(1));
	CObject *body = args->rest()->rest();

	//defn is always on top, so it doesnt pass context
	auto fn = Compiler::compileLambda(fnArgs, body,nullptr, env);
	env->bind(name, fn);

	return std::vector<Instruction>();
}

std::vector<Instruction> UnpackSyntax::getByteCode(CList* args, UserFunction* context, Environment* env)
{
	std::vector<Instruction> bc;
	for (auto it = args->getIterator(); it.hasNext();++it)
	{
		auto res = Compiler::compileItem(it.item(), context, env);
		bc.insert(bc.end(), res.begin(), res.end());
		bc.push_back(Instruction(UNPACK));
	}
	return bc;
}

std::vector<Instruction> BackquoteSyntax::getByteCode(CList* args, UserFunction* context, Environment* env)
{
	if (args->size() == 1 && args->first()->getType() == LIST)
		return getByteCode((CList *)args->first(),context,env);

	std::vector<Instruction> bc;
	for (auto it = args->getIterator(); it.hasNext(); ++it)
	{
		CObject *item = it.item();
		if (item->getType() == SYMBOL)
			bc.push_back(Instruction(PUSHS, memory.symbolTable().getSymbolAddress(static_cast<CSymbol*>(item))));
		else if(item->getType() == LIST)
		{
			CList *lst = (CList*)item;
			switch(lst->first()->getType())
			{
				case SYMBOL:
				{
					CObject *bind = env->getBinding((CSymbol*)lst->first());
					if (UnpackSyntax * syn = dynamic_cast<UnpackSyntax*>(bind))
					{
						auto res = syn->getByteCode(lst->rest(), context, env);
						bc.insert(bc.end(), res.begin(), res.end());
						break;
					}
					if(UnquoteSyntax * syn = dynamic_cast<UnquoteSyntax*>(bind))
					{
						auto res = syn->getByteCode(lst->rest(), context, env);
						bc.insert(bc.end(), res.begin(), res.end());
						break;
					}
				}
				default:
				{
					UserFunction *fn = new UserFunction(context->getArgs());
					fn->setContext(context);
					auto code = getByteCode(static_cast<CList*>(item), fn, env);
					code.push_back(Instruction(RETURN));
					fn->setBytecode(code);
					for (int i = 0; i < context->getArgs()->size(); i++)
						bc.push_back(Instruction(LOAD_LOCAL, i));
					bc.push_back(Instruction(PUSHC, context->constTable().putConstant(fn)));
					bc.push_back(Instruction(CALL, context->getArgs()->size()));
				}
			}
		}
		else	
			bc.push_back(Instruction(PUSHC, context->constTable().putConstant(item)));
	}
	bc.push_back(Instruction(PACK));

	return bc;
}

std::vector<Instruction> UnquoteSyntax::getByteCode(CList* args, UserFunction* context, Environment* env)
{
	return Compiler::compileItem(args->first(), context, env); //unquote takes only one argument. Maybe we should do this better way
}

DefMacroSyntax::DefMacroSyntax(IEvaluator* e) :eval(e){}

std::vector<Instruction> DefMacroSyntax::getByteCode(CList* args, UserFunction* context, Environment* env)
{
	CSymbol * name = static_cast<CSymbol *>(args->at(0));
	CList * maArgs = static_cast<CList*>(args->at(1));
	CObject *body = args->rest()->rest();

	UserFunction *fn = Compiler::compileLambda(maArgs, body, nullptr, env);

	auto mac = new Macro(fn,eval);
	env->bind(name, mac);

	return {};
}

std::vector<Instruction> ThisSyntax::getByteCode(CList* args, UserFunction* context, Environment* env)
{
	std::vector<Instruction> bc;
	bc.push_back(Instruction(THIS));
	return bc;
}