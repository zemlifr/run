#pragma once
#include "CObject.h"
#include "Clist.h"
#include "../runtime/Bytecode.h"
#include <vector>

class UserFunction;
class Environment;

class Syntax
	: public CObject
{
public:
	Syntax();
	virtual ~Syntax();
	void print(std::ostream& where) const override;
	Type getType() const override;
	CObject * eval(IEvaluator *e) override;
    void mark() override;
    void unmark() override;
};

class ByteCodeSyntax
  : public Syntax
{
public:
	virtual std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) = 0;
	CObject* clone() override;
};

class Lambda 
	: public ByteCodeSyntax
{
public:
    std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};

class SetSyntax
  : public ByteCodeSyntax
{
public:
    std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};

class IfSyntax
	: public ByteCodeSyntax
{
public:
	std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};

class QuoteSyntax
	: public ByteCodeSyntax
{
public:
	std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};

class DoSyntax
	: public ByteCodeSyntax
{
public:
	std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};


//posible syntatic sugar, maybe it could be replaced with non-native solution
//allows to write (defn fn (args) body) instead (set fn (lambda (args) body))
//also binds fn to environment directly, without creating set instructions.
class DefnSyntax
	: public ByteCodeSyntax
{
public:
	std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};

class UnpackSyntax
	: public ByteCodeSyntax
{
public:
	std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};

class BackquoteSyntax
	: public ByteCodeSyntax
{
public:
	std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};

class UnquoteSyntax
	: public ByteCodeSyntax
{
public:
	std::vector<Instruction> getByteCode(CList* args, UserFunction *context, Environment* env) override;
};

class DefMacroSyntax
	: public ByteCodeSyntax
{
public:
	DefMacroSyntax(IEvaluator *e);
	std::vector<Instruction> getByteCode(CList* args, UserFunction* context, Environment* env) override;

private:
	IEvaluator *eval;
};

class ThisSyntax
	: public ByteCodeSyntax
{
public:
	std::vector<Instruction> getByteCode(CList* args, UserFunction* context, Environment* env) override;
};