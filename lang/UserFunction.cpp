#include "UserFunction.h"

UserFunction::UserFunction(CList* args): context(nullptr), env(nullptr), args(args), closureMark(false)
{
	this->expectedArgs = args->size();
}

UserFunction::~UserFunction()
{
}

CObject* UserFunction::eval(IEvaluator* e)
{
	return this;
}

void UserFunction::mark()
{
  if (!marked)
  {
    marked = true;
    args->mark();
	consts.mark();
	if(env)
		env->mark();
  }
}

void UserFunction::unmark()
{
  marked = false;
  args->unmark();
  consts.unmark();
  if(env)
	env->unmark();
}

CObject* UserFunction::clone()
{
	//todo implement???
	return this;
}

void UserFunction::print(std::ostream& where) const
{
	where << "<user-function>";
}


Type UserFunction::getType() const
{
	return USER_FUNCTION;
}

int UserFunction::getLocalAdress(CSymbol* s)
{
	return args->indexOf(s);
}

void UserFunction::printBytecode(std::ostream& where) const
{
	for (auto it = bytecode.begin(); it != bytecode.end();++it)
	{
		where<<it->name()<<" "<<it->arg<<"\n";
	}
}

ConstTable& UserFunction::constTable()
{
	return consts;
}

UserFunction* UserFunction::getContext()
{
	return context;
}

void UserFunction::setContext(UserFunction* u)
{
	context = u;
}

void UserFunction::setClosureMark(bool mark)
{
	closureMark = mark;
}

bool UserFunction::hasClosureMark() const
{
	return closureMark;
}

Environment* UserFunction::buildEnvironment(Environment* parent, CList* bindValues)
{
	Environment *e = NEWENV(parent);
	if (bindValues->size() != expectedArgs)
		return e; //throw error here
	auto argIt = args->getIterator();
	auto bindIt = bindValues->getIterator();

	for (; argIt.hasNext() && bindIt.hasNext(); ++argIt, ++bindIt)
	{
		//do some check about symbols here
		e->bind((static_cast<CSymbol *>(argIt.item())), bindIt.item());
	}
	return e;
}

const std::vector<Instruction> UserFunction::getBytecode() const
{
	return bytecode;
}

void UserFunction::setBytecode(std::vector<Instruction> & bc)
{
	bytecode = bc;
}

CList* UserFunction::getArgs()
{
	return args;
}

void UserFunction::setEnvironment(Environment* environment)
{
	this->env = environment;
}

Environment* UserFunction::getEnvironment()
{
	return env;
}