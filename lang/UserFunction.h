#pragma once
#include "CFunction.h"
#include "Clist.h"
#include "../memory/Memory.h"
#include "../runtime/Bytecode.h"
#include "../memory/ConstTable.h"
#include "../runtime/Environment.h"

class UserFunction
	: public CFunction
{
public:
	UserFunction(CList* args);
	virtual ~UserFunction();
	CObject* eval(IEvaluator* e) override;
    void mark() override;
    void unmark() override;
	CObject* clone() override;

	void print(std::ostream& where) const override;
	Type getType() const override;
	int getLocalAdress(CSymbol * s);
	void printBytecode(std::ostream& where) const;
	ConstTable & constTable();
	UserFunction * getContext();
	void setContext(UserFunction *u);
	void setClosureMark(bool mark);
	bool hasClosureMark() const;

	Environment* buildEnvironment(Environment* parent, CList* bindValues);

	const std::vector<Instruction> getBytecode() const;
	void setBytecode(std::vector<Instruction> & bc);
	CList * getArgs();
	void setEnvironment(Environment* environment);
	Environment * getEnvironment();
private:
	//we need this for outer variables
	UserFunction * context;
	//closure will store binded outers here
	Environment *env;
	CList *args;
	ConstTable consts;
	std::vector<Instruction> bytecode;

	//help flag set by compiler to determine
	bool closureMark;
};


