#include "ConstTable.h"



ConstTable::ConstTable()
{
}


ConstTable::~ConstTable()
{
}

CObject* ConstTable::getConstatnt(int adr) const
{
	return values.at(adr);
}

int ConstTable::putConstant(CObject* co)
{
	int adr = indexOf(co);
	if (adr < 0)
	{
		values.push_back(co);
		return values.size() - 1;
	}
	return adr;
}

int ConstTable::indexOf(CObject* co)
{
	for (int i = 0; i < values.size();i++)
	{
		if (values.at(i) == co)
			return i;
	}

	return -1;
}

void ConstTable::mark()
{
	for (auto it = values.begin(); it != values.end();++it)
	{
		(*it)->mark();
	}
}

void ConstTable::unmark()
{
  for (auto it = values.begin(); it != values.end(); ++it)
  {
    (*it)->unmark();
  }
}