#pragma once
#include "../lang/CObject.h"
#include <vector>

class ConstTable
{
public:
	ConstTable();
	~ConstTable();
	CObject *getConstatnt(int adr) const;
	int putConstant(CObject *co);
	int indexOf(CObject *co);

	void mark();
  void unmark();

private:
	std::vector<CObject *> values;
};

