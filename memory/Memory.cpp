#include "Memory.h"
#include "../lang/CStream.h"
#include "../runtime/Stack.h"
#include "../runtime/VirtualMachine.h"
#include "../lang/CSequence.h"

Memory memory;

Memory::Memory() : nil(new CNil), t(new CBool(true)),f(new CBool(false)), sin(new StdIn), sout(new StdOut)
{
}


Memory::~Memory()
{
	for (auto it = objects.begin(); it != objects.end();++it)
	{
		delete *it;
	}
}

CString* Memory::newString(std::string val)
{
	CString *ret = new CString(val);
	objects.push_back(ret);
	return ret;
}

CLong* Memory::newLong(long long val)
{
	CLong *ret =  new CLong(val);
	objects.push_back(ret);
	return ret;
}

CFloat* Memory::newFloat(double val)
{
	CFloat * ret = new CFloat(val);
	objects.push_back(ret);
	return ret;
}

CSymbol* Memory::newSymbol(std::string val)
{
	CSymbol *symbol = symbols.getSymbol(val);
	if(!symbol)
	{
		symbol = new CSymbol(val);
		symbols.putSymbol(symbol);
	}
	return symbol;
}

CList* Memory::newList()
{
	CList *ret = new CList();
	objects.push_back(ret);
	return ret;
}

CStream* Memory::newStream(std::string path, bool append)
{
  CStream *fs = new FStream(path, append);
  objects.push_back(fs);
  return fs;
}

CSequence* Memory::newSequence(CLong* start, CLong* end, CLong* step)
{
	CSequence *seq = new CSequence(start, end, step);
	objects.push_back(seq);
	return seq;
}

CMap * Memory::newMap()
{
	CMap *map = new CMap();
	objects.push_back(map);
	return map;
}

CallFrame* Memory::newCallFrame(CallFrame* parent)
{
	CallFrame *ret = new CallFrame(parent);
	objects.push_back(ret);
	return ret;
}

Environment* Memory::newEnvironment(Environment* parent)
{
	Environment *ret = new Environment(parent);
	objects.push_back(ret);
	return ret;
}

void Memory::addIStackGetter(IStackGetter* istack)
{
  this->istack = istack;
}

SymbolTable& Memory::symbolTable()
{
	return symbols;
}

void Memory::registerObject(CObject* obj)
{
	objects.push_back(obj);
}

void Memory::gc()
{
  markAll();
  sweep();
  unmarkAll();
}

void Memory::markAll()
{
  Environment::top()->mark();
  istack->getStack().mark();
}

void Memory::unmarkAll()
{
  Environment::top()->unmark();
  istack->getStack().unmark();
}

void Memory::sweep()
{
    std::vector<CObject *> usedObjects;
    for (auto it = objects.begin(); it != objects.end(); ++it)
    {
        if (!(*it)->isMarked())
        {
            delete *it;
        }
        else
        {
            usedObjects.push_back(*it);
        }
    }
    objects = usedObjects;
}

CNode *Memory::newNode() {
	CNode *node = new CNode();
	objects.push_back(node);
	return node;
}