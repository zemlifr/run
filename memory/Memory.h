#pragma once
#include "../lang/Atoms.h"
#include "../lang/Clist.h"
#include "SymbolTable.h"
#include "../lang/CStream.h"
#include "../lang/CMap.h"
#include "../runtime/IStackGetter.h"

class CallFrame;
class Environment;
class CSequence;

class Memory
{
public:
	Memory();
	~Memory();

	CString * newString(std::string val);
	CLong * newLong(long long val);
	CFloat * newFloat(double val);
	CSymbol * newSymbol(std::string val);
	CList * newList();
	CNode * newNode();
    CStream * newStream(std::string path, bool append);
	CSequence * newSequence(CLong* start, CLong* end, CLong* step);
	CMap * newMap();
	CallFrame * newCallFrame(CallFrame *parent);
	Environment * newEnvironment(Environment *parent);
    void addIStackGetter(IStackGetter *istack);
	SymbolTable & symbolTable();
	void registerObject(CObject * obj);
	void gc();

	CNil *nil;
	CBool *t;
	CBool *f;
	CStream *sin;
	CStream *sout;

private:
	SymbolTable symbols;
	//tempoary until GC implemented stores all obejcts
	std::vector<CObject *> objects;
  void markAll();
  void unmarkAll();
  void sweep();
  IStackGetter *istack;
};

extern Memory memory;

#define NEWLONG(x) memory.newLong(x)
#define NEWFLOAT(x) memory.newFloat(x)
#define NEWSTRING(x) memory.newString(x)
#define NEWSYMBOL(x) memory.newSymbol(x)
#define NEWLIST memory.newList()
#define NEWNODE memory.newNode()
#define NEWSTREAM(x, y) memory.newStream(x, y)
#define NEWSEQUENCE(x,y,z) memory.newSequence(x,y,z)
#define NEWENV(x) memory.newEnvironment(x)
#define NEWMAP memory.newMap()
#define NEWCALLFRAME(x) memory.newCallFrame(x)
#define NIL memory.nil
#define TRUE memory.t
#define FALSE memory.f
#define STDIN memory.sin
#define STDOUT memory.sout