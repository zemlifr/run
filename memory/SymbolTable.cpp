#include "SymbolTable.h"
#include "../lang/Atoms.h"


SymbolTable::SymbolTable()
{
}


SymbolTable::~SymbolTable()
{
	for (auto it = symbols.begin(); it != symbols.end();++it)
	{
		delete *it;
	}
}

void SymbolTable::putSymbol(CSymbol* sym)
{
	symbols.push_back(sym);
}

CSymbol* SymbolTable::getSymbol(std::string val) const
{
	for (auto it = symbols.begin(); it != symbols.end();++it)
	{
		if ((*it)->getVal() == val)
			return *it;
	}
	return nullptr;
}

CSymbol* SymbolTable::getSymbol(int address) const
{
  return symbols.at(address);
}

int SymbolTable::getSymbolAddress(CSymbol* sym) const
{
	int adr = 0;

	for (auto it = symbols.begin(); it != symbols.end(); ++it, adr++)
	{
		if ((*it) == sym)
			return adr;
	}

	return -1;
}
