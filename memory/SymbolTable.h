#pragma once
#include <string>
#include <vector>
class CSymbol;

class SymbolTable
{
public:
	SymbolTable();
	~SymbolTable();
	void putSymbol(CSymbol *sym);
	CSymbol *getSymbol(std::string val) const;
    CSymbol *getSymbol(int address) const;
	int getSymbolAddress(CSymbol *sym) const;
	//TODO consider better implementation without vector
private:
	std::vector<CSymbol *> symbols;
};

