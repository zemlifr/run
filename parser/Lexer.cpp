#include "Lexer.h"

#include <iostream>
#include <cctype>
#include <sstream>

const char Lexer::LIST_OPEN = '(';
const char Lexer::LIST_CLOSE = ')';
const char Lexer::QUOTE = '\'';
const char Lexer::BACQUOTE = '\`';
const char Lexer::UNQUOTE = '\,';
const char Lexer::UNFOLD = '@';
const char Lexer::MAP_OPEN='{';
const char Lexer::MAP_CLOSE='}';
const char Lexer::COMMENT = '#';
const int MAX_LINE_LENGTH = 65536;

std::string SBACQUOTE = "`";
std::string SUNQUOTE = ",";
std::string SUNFOLD = "@";
std::string SQUOTE = "quote";

Lexer::Lexer() : input(std::cin)
{
	head = read();
}

Lexer::Lexer(std::istream& in) : input(in)
{
	head = read();
}

Lexer::~Lexer()
{
}

Token Lexer::getToken()
{
	return head;
}

void Lexer::readNext()
{
	head = read();
}

void Lexer::flush()
{
	input.clear();
}

Token Lexer::read()
{
	while (input.get(in))
	{
		if (isspace(in))
		{
			skipWhitespaces();
		}
		else if (in == COMMENT)
		{
			skipComments();
		}
		else if(in==LIST_OPEN)
		{
			return Token(Token::LIST_OPEN);
		}
		else if(in==LIST_CLOSE)
		{
			return Token(Token::LIST_CLOSE);
		}
		else if (in == MAP_OPEN)
		{
			return Token(Token::MAP_OPEN);
		}
		else if (in == MAP_CLOSE)
		{
			return Token(Token::MAP_CLOSE);
		}
		else if (in == QUOTE)
		{
			return Token(Token::SPECIAL,SQUOTE);
		}
		else if (in == UNFOLD)
		{
			return Token(Token::SPECIAL,SUNFOLD);
		}
		else if(in == BACQUOTE)
		{
			return Token(Token::SPECIAL, SBACQUOTE);
		}
		else if(in == UNQUOTE)
		{
			return Token(Token::SPECIAL, SUNQUOTE);
		}
		else if(in=='"')
		{
			return readStringToken();
		}
		else if(isSymbolHead(in))
		{
			return readSymbol();
		}
		else if(isdigit(in))
		{
			return readNumber(false);
		}
	}

	return Token(Token::END);
}

void Lexer::skipWhitespaces()
{
	while (input.get(in) && isspace(in));

	input.putback(in);
}

void Lexer::skipComments()
{
	input.ignore(MAX_LINE_LENGTH, '\n');
}

Token Lexer::readStringToken()
{
	std::stringstream buffer;
  bool escape = false;
	while(input.get(in) && in != '"')
	{
    if (escape)
    {
      if (isEscapeChar(in))
      {
        buffer << getEscapeChar(in);
      }
      else
      {
        buffer << '\\';
        buffer << in;
      }
      escape = false;
    }
    else
    {
      if (in == '\\')
      {
        escape = true;
      }
      else
      {
        buffer << in;
      }
    }    
	}
	return Token(Token::STRING, buffer.str());
}

Token Lexer::readHexa()
{

	return Token(Token::LONG, "");
}

bool Lexer::isSymbolHead(char c)
{
	return isalpha(c) ||
		c == '_' ||
		c == '-' ||
		c == '+' ||
		c == '=' ||
		c == '*' ||
		c == '&' ||
		c == '/' ||
		c == '<' ||
		c == '>' ||
		c == '?' ;
}

bool Lexer::isEscapeChar(char c)
{
  return c == 't' ||
    c == 'n' ||
    c == 'r' ||
    c == '\\' ||
    c == '"';
}

char Lexer::getEscapeChar(char c)
{
  switch (c)
  {
  case 't':
    return '\t';
  case 'r':
    return '\r';
  case 'n':
    return '\n';
  case '\\':
    return '\\';
  case '"':
    return '"';
  }
  return '\0';
}

Token Lexer::readNumber(bool sign)
{
	std::stringstream buffer;
	
	if (sign)
		buffer << "-";

	buffer << in; //first number

	while (input.get(in) && isdigit(in))
		buffer << in;

	//read float
	if (in == '.')
	{
		//TODO: move to own method, allow scientific notation
    buffer << in;
    input.get(in);
    bool lazyList = false;
    if (in == '.') //lazy list detected
    {
      lazyList = true;
      buffer << in;
    }
    else
    {
      input.putback(in);
    }
		while (input.get(in) && isdigit(in))
			buffer << in;

		input.putback(in);
    if (lazyList)
    {
      input.get(in);
      if (in == ':')
      {
        buffer << in;
        while (input.get(in) && isdigit(in))
          buffer << in;
      }
      input.putback(in);
      return Token(Token::LAZY_LIST, buffer.str());
    }
    else
		  return Token(Token::FLOAT, buffer.str());
	}
	 input.putback(in);
	 //return int
		return Token(Token::LONG, buffer.str());
}

Token Lexer::readSymbol()
{
	std::stringstream buffer;
	
	buffer << in;
	
	//check if not negtive number
	if(in == '-')
	{
		if (input.get(in) && isdigit(in))
			//return negative number instead
			return readNumber(true);

			input.putback(in);
	}
	else if (in == '=')
	{
		if (input.get(in) && in == '>')
			return Token(Token::BIND, "=>");

			input.putback(in);
	}

	while (input.get(in) && (isSymbolHead(in) || isdigit(in)))
		buffer << in;

	input.putback(in);
	//TODO: check for keywords?
	return Token(Token::SYMBOL, buffer.str());
}
