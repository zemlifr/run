#pragma once

/*
ACCEPTED GRAMMAR IN EBNF:

STRING: '"' ( ~'"' | '\\' '"' )* '"' ;
SYMBOL:
LONG:
BOOL: 'true' | 'false'
TNIL: 'nil';
FLOAT:
LIST_OPEN: '(';
LIST_CLOSE: ')';

WS: ( '\t' | ' ' | '\r' | '\n'| '\u000C' | ',')+;
COMMENT: ';' ~('\r' | '\n')* ;
*/

#include <istream>

#include "Token.h"

class Lexer
{
public:
	static const char LIST_OPEN;
	static const char LIST_CLOSE;
	static const char MAP_OPEN;
	static const char MAP_CLOSE;
	static const char QUOTE;
	static const char UNFOLD;
	static const char BACQUOTE;
	static const char UNQUOTE;
	static const char COMMENT;


	Lexer();
	Lexer(std::istream & in);
	~Lexer();
	Token getToken();
	void readNext();
	void flush();
private:
	Token readStringToken();
	bool isSymbolHead(char c);
  bool isEscapeChar(char c);
  char getEscapeChar(char c);
	Token readHexa();
	Token readNumber(bool sign);
	Token readSymbol();
	Token read();
	void skipWhitespaces();
	void skipComments();

	char in;
	Token head;
	std::istream & input;
};

