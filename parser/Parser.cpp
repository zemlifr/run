#include "Parser.h"

#include "../memory/Memory.h"
#include "../util/Exceptions.h"


Parser::Parser(const Lexer& lex) : lexer(lex)
{
}

Parser::~Parser()
{
}

CObject* Parser::parse()
{
	Token peek = lexer.getToken();
  //std::cout << peek.getTypeString() << "> " << peek.getText() << std::endl;
	switch(peek.getType())
	{
	case Token::STRING:
			return parseString();
	case Token::SYMBOL:
	case Token::BIND:
			return parseSymbol();
	case Token::LONG:
			return parseLong();
	case Token::FLOAT:
			return parseFloat();
	case Token::LIST_OPEN: 
			return parseList();
	case Token::MAP_OPEN:
			return parseMap();
	case Token::SPECIAL:
		return parseSpecial();
	case Token::END:
		return NIL;
	case Token::LAZY_LIST:
		return parseLazyList();
	default: break;
	}
	throw ParseError("Unexpected token: "+peek.getTypeString());
}

Lexer& Parser::getLexer()
{
  return lexer;
}

CObject* Parser::parseList()
{
	lexer.readNext(); //consume left bracket

	if (lexer.getToken().getType() == Token::LIST_CLOSE)
		return NEWLIST;
	//Empty NIL here maybe if list is empty?
	CList *list = NEWLIST;

	do{
		//read until list end
		list->insertBack(parse());
		lexer.readNext();
  } while (lexer.getToken().getType() != Token::LIST_CLOSE);

	return list;
}

CObject* Parser::parseLazyList()
{
  Token peek = lexer.getToken();
  std::string lazyList = peek.getText();
  CLong* start = NEWLONG(0);
  CLong* end = NEWLONG(0);
  CLong* step = NEWLONG(1);
  auto dot = lazyList.find('.');
  start->setVal(std::stol(lazyList.substr(0, dot)));
  auto colon = lazyList.find(':');
  if (colon != std::string::npos)
  {
    step->setVal(std::stol(lazyList.substr(colon + 1)));
    end->setVal(std::stol(lazyList.substr(dot + 2, colon - dot - 1)));
  }
  else
  {
    end->setVal(std::stol(lazyList.substr(dot + 2)));
  }
  CList *uList = NEWLIST;
  uList->insertBack(NEWSYMBOL("seq"));
  uList->insertBack(start);
  uList->insertBack(end);
  uList->insertBack(step);
  return uList;
}

CObject * Parser::parseMap()
{
	lexer.readNext(); //consume left bracket

	if (lexer.getToken().getType() == Token::MAP_CLOSE)
		return NEWMAP;
	//Empty NIL here maybe if list is empty?
	CMap *map = NEWMAP;

	do {
		//read until list end
		CObject * key = parse();
		lexer.readNext();
		if (lexer.getToken().getType() != Token::BIND)
			throw ParseError("Unexpected token: " + lexer.getToken().getTypeString());
		lexer.readNext();
		CObject * val = parse();
		lexer.readNext();
		map->put(key, val);
	} while (lexer.getToken().getType() != Token::MAP_CLOSE);

	return map;
}

CObject* Parser::parseLong()
{
	//TODO: support another radixes
	long long val = stoll(lexer.getToken().getText());
	return NEWLONG(val);
}

CObject* Parser::parseFloat()
{
	double val = stod(lexer.getToken().getText());
	return NEWFLOAT(val);
}

CObject* Parser::parseSymbol()
{
	return NEWSYMBOL(lexer.getToken().getText());
}

CObject* Parser::parseSpecial()
{
	auto t = lexer.getToken().getText();
	lexer.readNext(); //consume quote symbol
	CList *slist = NEWLIST;
	slist->insertBack(NEWSYMBOL(t));
	slist->insertBack(parse());
	return slist;
}

CObject* Parser::parseString()
{
	return NEWSTRING(lexer.getToken().getText());
}