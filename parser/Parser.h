#pragma once

#include "../lang/CObject.h"

#include "Lexer.h"

class Parser
{
public:
	Parser(const Lexer & lex);
	~Parser();
	CObject * parse();
    Lexer& getLexer();

private:
	CObject * parseList();
    CObject * parseLazyList();
	CObject * parseMap();
	CObject* parseString();
	CObject* parseLong();
	CObject* parseFloat();
	CObject* parseSymbol();
	CObject* parseSpecial();

	//members

	Lexer lexer;
};

