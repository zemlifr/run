#pragma once

class Token
{
public:
	enum Type {STRING,SYMBOL,LONG,FLOAT,LIST_OPEN,LIST_CLOSE, MAP_OPEN, MAP_CLOSE, BIND, LAZY_LIST, END, SPECIAL};
	
	Token() : type(END){};
	
	Token(Type type, std::string text="") : type(type),text(text){};

	Type getType() const
	{
		return type;
	}

  std::string getTypeString() const
  {
    switch (getType())
    {
    case STRING:
      return "STRING";
    case SYMBOL:
      return "SYMBOL";
    case LONG:
      return "LONG";
    case FLOAT:
      return "FLOAT";
    case LIST_OPEN:
      return "LIST_OPEN";
    case LIST_CLOSE:
      return "LIST_CLOSE";
    case MAP_OPEN:
      return "MAP_OPEN";
    case MAP_CLOSE:
      return "MAP_CLOSE";
    case BIND:
      return "BIND";
    case END:
      return "END";
	case SPECIAL:
	  return "SPECIAL";
    default:
      return "unknown";
    }//,FLOAT,LIST_OPEN,LIST_CLOSE, MAP_OPEN, MAP_CLOSE, QUOTE, BIND, END
  }

	std::string getText() const
	{
		return text;
	}

private:
	Type type;
	std::string text;
};

