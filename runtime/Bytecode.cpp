#include "Bytecode.h"

char const *BcSymbols[] = {
	"NOP",
	"CALL",
	"JUMP",
	"JUMP_FALSE",
	"STORE",
	"STORE_LOCAL",
	"STORE_OUTER",
	"LOAD",
	"LOAD_LOCAL",
	"LOAD_OUTER",
	"RETURN",
	"PUSHC",
	"PUSHS",
	"UNPACK",
	"PACK",
	"THIS"
};