#pragma once

enum Bytecode
{
	NOP,
	CALL,
	JUMP,
	JUMP_FALSE,
	STORE,
	STORE_LOCAL,
	STORE_OUTER,
	LOAD,
	LOAD_LOCAL,
	LOAD_OUTER,
	RETURN,
	PUSHC, // push constant
	PUSHS, // push symbol
	UNPACK, /*if top of stack is LIST object, unpack its items to stack
		    this must be done by instruction, because stack is cleaned after function call*/
	PACK, //opposite to unpack - take everything at stack for current frame (- locals) and make list
	THIS //push actual function to stack. Allows lambda recursion
};

extern char const *BcSymbols[];

struct Instruction
{
	Bytecode bc;
	int arg;
	Instruction(Bytecode bc=NOP, int arg=0) : bc(bc), arg(arg){}
	const char * name() const{ return BcSymbols[bc]; }
};