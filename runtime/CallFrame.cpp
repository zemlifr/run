#include "CallFrame.h"


CallFrame::CallFrame(CallFrame* parent) : pc(0), bp(0), parent(parent)
{
}

CallFrame::~CallFrame()
{
}

CObject* CallFrame::eval(IEvaluator* e)
{
	return this;
}

void CallFrame::mark()
{
	marked = true;
	if (parent)
		parent->mark();
	if (f)
		f->mark();
	if (e)
		e->mark();
}

void CallFrame::unmark()
{
	marked = false;
	if (parent)
		parent->unmark();
	if (f)
		f->unmark();
	if (e)
		e->unmark();
}

void CallFrame::print(std::ostream& where) const
{
	where << "<CALL-FRAME>";
}

void CallFrame::setPc(int pc)
{
	this->pc = pc;
}

int CallFrame::getPc() const
{
	return pc;
}

void CallFrame::setBp(int sp)
{
	this->bp = sp;
}

int CallFrame::getBp() const
{
	return bp;
}

void CallFrame::setFunction(UserFunction* fn)
{
	this->f = fn;
}

UserFunction* CallFrame::getFunction()
{
	return f;
}

void CallFrame::setEnvironment(Environment* e)
{
	this->e = e;
}

Environment* CallFrame::getEnvironment()
{
	return e;
}

CallFrame* CallFrame::getParent()
{
	return parent;
}