#pragma once
#include "../lang/UserFunction.h"

class CallFrame : 
	public CObject
{
public:
	CallFrame(CallFrame *parent = nullptr);
	~CallFrame();
	CObject* eval(IEvaluator* e) override;
	void mark() override;
	void unmark() override;
	void print(std::ostream& where) const override;
	void setPc(int pc);
	int getPc() const;
	void setBp(int sp);
	int getBp() const;
	void setFunction(UserFunction * fn);
	UserFunction * getFunction();
	void setEnvironment(Environment *e);
	Environment *getEnvironment();
	CallFrame *getParent();
	

private:
	int pc;
	int bp;
	UserFunction *f;
	Environment *e;
	CallFrame *parent;
};

