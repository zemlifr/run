#include "Compiler.h"
#include "../lang/Clist.h"
#include "../lang/UserFunction.h"
#include "../lang/Syntax.h"
#include "../lang/Macro.h"
#include "../util/Exceptions.h"


Compiler::Compiler()
{
}


Compiler::~Compiler()
{
}

UserFunction* Compiler::compileLambda(CList* args, CObject* body, UserFunction *context, Environment* env)
{
	if (!args->isPure(SYMBOL))
		return nullptr; //throw exception, compile error whatever 

	//use Memory call
	UserFunction *fn = new UserFunction(args);
	fn->setContext(context);
	std::vector<Instruction> bc;

	if(body->getType()!=LIST)
		bc = compileItem(body,fn, env);
	else
	{
		CList * list = static_cast<CList *>(body);
		for (auto it = list->getIterator(); it.hasNext(); ++it)
		{
			auto res = compileItem(it.item(), fn, env);
			bc.insert(bc.end(), res.begin(), res.end());
		}
	}

	bc.push_back(Instruction(RETURN, 0));
	fn->setBytecode(bc);
	return fn;
}

std::vector<Instruction> Compiler::compileList(CList* list, UserFunction* context, Environment* env)
{
  if (list->first()->getType() == SYMBOL)
  {
    CSymbol *symbol = static_cast<CSymbol*>(list->first());
    CObject *boundObject = env->getBinding(symbol);
    switch (boundObject->getType())
	{
		case MACRO:
		{
			auto res =((Macro*)boundObject)->apply(list->rest(), env);
			std::cout << std::endl <<*res << std::endl;
			return compileItem(res, context, env);
		}
		case SYNTAX:
			return static_cast<ByteCodeSyntax *>(boundObject)->getByteCode(list->rest(),context,env);
		default:
			break;
    
    }
  }
  return compileFunctionCall(list, context, env);
}

std::vector<Instruction> Compiler::compileItem(CObject* obj, UserFunction* context, Environment* env)
{
	std::vector<Instruction> bc;
  if (obj == nullptr)
    throw RuntimeError("Compiling null object.");
	switch (obj->getType())
	{
	case SYMBOL:
	{
		int idx = context->getLocalAdress(static_cast<CSymbol*>(obj));
		if (idx < 0)
		{
			CSymbol * sym = static_cast<CSymbol *>(obj);
			idx = memory.symbolTable().getSymbolAddress(sym);
			if (isOuter(sym, context))
			{
				context->setClosureMark(true);
				bc.push_back(Instruction(LOAD_OUTER, idx));
			}
			else
				bc.push_back(Instruction(LOAD, idx));

		}
		else
			bc.push_back(Instruction(LOAD_LOCAL, idx));
		break;
	}
	case LIST:
	{
		auto res = compileList(static_cast<CList*>(obj),context, env);
		bc.insert(bc.end(), res.begin(), res.end());
		break;
	}
	case LONG:
	case FLOAT:
	case STRING:
	case BOOL:
	case USER_FUNCTION:
	case MAP:
	{
		bc.push_back(Instruction(PUSHC, context->constTable().putConstant(obj)));
	}
	default: break;
	}

	return bc;
}

std::vector<Instruction> Compiler::compileFunctionCall(CList* list, UserFunction* context, Environment* env)
{
  std::vector<Instruction> bc;
  for (auto it = list->rest()->getIterator(); it.hasNext(); ++it)
  {
    auto res = compileItem(it.item(), context, env);
    bc.insert(bc.end(), res.begin(), res.end());
  }

  auto res = compileItem(list->first(), context, env);
  bc.insert(bc.end(), res.begin(), res.end());
  if (!list->isEmpty())
    bc.push_back(Instruction(CALL, list->rest()->size())); // as argument use count of given args
  return bc;
}

std::vector<Instruction> Compiler::compileMacroList(Macro* mac, CList* list, UserFunction* fn, Environment* environment)
{
	std::vector<Instruction> bc;
	for (auto it = list->getIterator(); it.hasNext(); ++it)
	{
		bc.push_back(Instruction(PUSHC, fn->constTable().putConstant(it.item())));
	}

	bc.push_back(Instruction(PUSHC, fn->constTable().putConstant(mac->getFunc())));
	bc.push_back(Instruction(CALL, list->size()));
	return bc;
}

/**
 * Helper function which determines, wheter symbol is local variable name in outer functions
 */
bool Compiler::isOuter(CSymbol* sym, UserFunction* context)
{
	//outer is function in which is currently parsed function defined
	auto outer = context->getContext();
	while (outer != nullptr)
	{
		if (outer->getLocalAdress(sym) >= 0)
			return true;
		outer = outer->getContext();
	}

	return false;
}