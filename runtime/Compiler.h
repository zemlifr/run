#pragma once
#include <vector>
#include "Bytecode.h"
#include "Environment.h"
class Macro;
class CObject;
class CList;
class UserFunction;

class Compiler
{
public:
	Compiler();
	~Compiler();
	static UserFunction * compileLambda(CList *args, CObject * body, UserFunction * context, Environment* env);
	static std::vector<Instruction> compileList(CList* list, UserFunction* context, Environment* env);
	static std::vector<Instruction> compileItem(CObject *obj, UserFunction* context, Environment* env);
	static std::vector<Instruction> compileFunctionCall(CList *list, UserFunction* context, Environment* env);
	static std::vector<Instruction> compileMacroList(Macro* mac, CList* list, UserFunction* fn, Environment* environment);
	static bool isOuter(CSymbol* sym, UserFunction* context);

};

