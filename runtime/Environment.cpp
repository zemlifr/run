#include "Environment.h"

#include "../memory/Memory.h"

Environment * Environment::topEnvironment = new Environment();

Environment::Environment(Environment *parent) : parent(parent){

}

Environment::Environment() : parent(nullptr)
{
  marked = true;
}

Environment::~Environment()
{
	/*for(auto iterator = bindings.begin(); iterator != bindings.end(); ++iterator) {
		delete iterator->second;
	}*/
}

void Environment::bind(CSymbol* key, CObject* val)
{
	bindings[key] = val;
}

void Environment::removeBinding(CSymbol* key)
{
  bindings.erase(key);
}

Environment* Environment::getParent() const
{
	return parent;
}

CObject* Environment::eval(IEvaluator* e)
{
	return this;
}

void Environment::mark()
{
  marked = true;
  for (auto it = bindings.begin(); it != bindings.end();++it)
  {
    it->first->mark();
    it->second->mark();
  }
  if (parent && !parent->isMarked())
	  parent->mark();
}

void Environment::unmark()
{
	marked = false;
  for (auto it = bindings.begin(); it != bindings.end(); ++it)
  {
    it->first->unmark();
    it->second->unmark();
  }
  if (parent && parent->isMarked())
	  parent->unmark();
}

void Environment::print(std::ostream& where) const
{
	//print environment?
}

Environment* Environment::top()
{
	return topEnvironment;
}

int Environment::size() const
{
	return bindings.size();
}

CObject * Environment::getBinding(CSymbol* key) const
{
	const Environment *env = this;

	while(env)
	{
		auto it = env->bindings.find(key);
		if (it != env->bindings.end())
			return it->second;

		env = env->getParent();
	}
	return NIL;
}

