#pragma once
#include "../lang/CObject.h"
#include <map>

class CSymbol;

class Environment
	: public CObject
{
public:
	Environment(Environment * parent);
	~Environment();
	void bind(CSymbol *key, CObject * val);
	CObject * getBinding(CSymbol *key) const;
	void removeBinding(CSymbol *key);
	Environment * getParent() const;
	static Environment * top();
	int size() const;


	CObject* eval(IEvaluator* e) override;
	void mark() override;
  void unmark() override;
	void print(std::ostream& where) const override;
private:
	Environment();

	Environment *parent;

	std::map<CSymbol *, CObject*> bindings;
	
	static Environment* topEnvironment;
};

