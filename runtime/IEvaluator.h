#pragma once
#include "../lang/CObject.h"

class Macro;
class SetSyntax;
class Lambda;
class CList;
class CSymbol;

class IEvaluator
{
public:
	virtual ~IEvaluator(){}
	virtual CObject * eval(CSymbol *) = 0;
	virtual CObject * eval(CList *) = 0;
	virtual CObject * eval(Macro *m, CList *) = 0;
};
