#pragma once


class Stack;

class IStackGetter
{
public:
  virtual ~IStackGetter() {};
  virtual Stack getStack() = 0;
};
