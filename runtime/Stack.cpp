#include "Stack.h"

#include "../memory/Memory.h"


Stack::Stack() : data(DEFAULT_SIZE), sp(0), size(DEFAULT_SIZE)
{
}


Stack::~Stack(){}

void Stack::push(CObject* obj)
{
	data[sp] = obj;
	sp++;
}

void Stack::drop(int count)
{
	sp -= count;
}

CObject* Stack::pop()
{
	if(!isEmpty())
		return data[--sp];

	return NIL;
}

bool Stack::isEmpty() const
{
	return sp <= 0;
}

int Stack::getSp() const
{
	return sp;
}

void Stack::mark()
{
  for (int i = 0; i < sp;i++)
  {
	  data[i]->mark();
  }
}

void Stack::unmark()
{
  for (int i = 0; i < sp; i++)
  {
    data[i]->unmark();
  }
}

CObject* & Stack::operator[](int index)
{
	if(index >=0 && index<size)
		return data[index];
	
	//TODO: throw some exception
	return data[sp];
}