#pragma once
#include "../lang/CObject.h"
#include <vector>

//TODO: Add stack resize and overflow detection

class Stack
{
public:
	static const int DEFAULT_SIZE = 100;

	Stack();
	~Stack();
	void push(CObject *obj);
	void drop(int count);
	CObject * pop();
	bool isEmpty() const;
	int getSp() const;
  void mark();
  void unmark();
	CObject * & operator[](int index);
  
private:
	std::vector<CObject *> data;
	int sp;
	int size;
};

