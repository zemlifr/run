#include "VirtualMachine.h"
#include "../lang/Atoms.h"
#include "../memory/Memory.h"
#include "../builtin/BuiltinMath.h"
#include "../lang/Syntax.h"
#include "../lang/UserFunction.h"
#include "Compiler.h"
#include "../builtin/BuiltinControl.h"
#include "../builtin/BuiltinList.h"
#include "../builtin/BuiltinStreams.h"
#include "../parser/Parser.h"
#include "../util/Exceptions.h"


VirtualMachine::VirtualMachine() : environment(Environment::top())
{
    initTopEnvironment();
    memory.addIStackGetter(this);
    includeStdLib();
}


VirtualMachine::~VirtualMachine()
{
    delete environment;
}

void VirtualMachine::run()
{
    running = true;
	CObject *result;

	while (running)
	{
		std::cout << "input>";
		CObject *exp = reader.read();
		try
		{
			result = exp->eval(this);
		}
		catch (RuntimeError e)
		{
			result = NIL;
			std::cout << "Error:" << e.what();
			stack.drop(stack.getSp());
		}

        std::cout << *result << std::endl;
        memory.gc();
    }
}

void VirtualMachine::stop()
{
    running = false;
}

CObject *VirtualMachine::evalBytecode(CallFrame *frame)
{
    UserFunction *fn = frame->getFunction();
    auto bc = fn->getBytecode();
    int pc = 0;
    int bp = 0;

    Environment *env = frame->getEnvironment();
    //fn->printBytecode(std::cout);
    while (true)
    {
        if (bc.size() <= pc)
            break;
        Bytecode op = bc[pc].bc;
        switch (op)
        {
            case CALL:
            {
                CObject *call = stack.pop();
                int arg = bc[pc].arg;
                if (call->getType() == BUILTIN_FUNCTION)
                {
                    //call builtin
                    CList *params = NEWLIST;
                    BuiltinFunction *bf = static_cast<BuiltinFunction *>(call);
                    for (int i = 0; i < arg; i++)
                    {
                        params->insertFront(stack.pop());
                    }
                    stack.push(bf->operator()(params));
                    pc++;
					break;
                }
				else if (call->getType() == USER_FUNCTION)
                {
                    //ITS TAILCALL
                    if (call == fn && pc == bc.size() - 2 && bc[bc.size() - 1].bc == RETURN)
                    {
                        //reload arguments
                        int ac = fn->expectedArgsCount();

                        for (int i = 0; i < ac; i++)
                        {
                            CObject *val = stack.pop();
                            stack[bp + ac - i - 1] = val;
                        }
                        //jump to start
                        pc = 0;
                        break;
                    } 
					else
                    {
						fn = static_cast<UserFunction *>(call);
						if (fn->expectedArgsCount() != arg)
							throw RuntimeError("Function called with wrong arguments count.");
                        //save old values
                        pc++;
                        frame->setPc(pc);
                        frame->setBp(bp);
                        //set new values
                        CallFrame *newFrame = NEWCALLFRAME(frame);
                        pc = 0;
                        bp = stack.getSp() - arg;
                        newFrame->setFunction(fn);
                        newFrame->setBp(bp);
                        bc = fn->getBytecode();
						if (fn->getEnvironment())
							{
								newFrame->setEnvironment(fn->getEnvironment());
								env = fn->getEnvironment();
							}
						else
							 newFrame->setEnvironment(env);

                        frame = newFrame;
                        
                    }
                }
				else
					throw RuntimeError("Invalid call");
                break;
            }
            case JUMP:
            {
                pc += bc[pc].arg;
                break;
            }
            case JUMP_FALSE:
            {
                CObject *obj = stack.pop();
                if (!obj->isTrue())
                    pc += bc[pc].arg;
                else
                    pc++;
                break;
            }
            case STORE_OUTER:
            {
                CSymbol *sym = memory.symbolTable().getSymbol(bc[pc].arg);
                int adr = computeAbsoluteOuterAdress(sym, frame);
                if (adr >= 0) //this is kind of hack. if outer variable does not exists, look to environment
                {
                    stack[adr] = stack.pop();
                    pc++;
                    break;
                }
            }
            case STORE:
            {
                CSymbol *sym = memory.symbolTable().getSymbol(bc[pc].arg);
                env->bind(sym, stack.pop());
                pc++;
                break;
            }
            case STORE_LOCAL:
            {
                stack[bp + bc[pc].arg] = stack.pop();
                pc++;
                break;
            }
            case LOAD_OUTER:
            {
                CSymbol *sym = memory.symbolTable().getSymbol(bc[pc].arg);
                int adr = computeAbsoluteOuterAdress(sym, frame);
                if (adr >= 0) //this is kind of hack. if outer variable does not exists, look to environment
                {
                    stack.push(stack[adr]);
                    pc++;
                    break;
                }
            }
            case LOAD:
            {
                CSymbol *sym = memory.symbolTable().getSymbol(bc[pc].arg);
                stack.push(env->getBinding(sym));
                pc++;
                break;
            }
            case LOAD_LOCAL:
            {
                stack.push(stack[bp + bc[pc].arg]);
                pc++;
                break;
            }
            case RETURN:
            {
                //return to function
                if (frame->getParent())
                {
                    CObject *ret = stack.pop();
                    if (ret->getType() == USER_FUNCTION)
                    {
                        UserFunction *fun = static_cast<UserFunction *>(ret);
                        if (fun->hasClosureMark())
                            ret = makeClosure(fun, frame);
                    }
                    stack.drop(stack.getSp() - bp);
                    stack.push(ret);
                    frame = frame->getParent();
                    pc = frame->getPc();
                    bp = frame->getBp();
                    fn = frame->getFunction();
                    bc = fn->getBytecode();
                    env = frame->getEnvironment();
                } else //return to top
                {
                    return stack.pop();
                }
                break;
            }
            case PUSHC:
            {
                CObject *co = fn->constTable().getConstatnt(bc[pc].arg);
                stack.push(co);
                pc++;
                break;
            }
            case PUSHS:
            {
                CSymbol *sym = memory.symbolTable().getSymbol(bc[pc].arg);
                stack.push(sym);
                pc++;
                break;
            }
            case UNPACK:
            {
                CObject *obj = stack.pop();
                if (obj->getType() == LIST)
                {
                    ISequence *lst = static_cast<ISequence *>(obj);
                    auto it = lst->iterator();
                    for (; it->hasNext(); ++(*it))
                    {
                        stack.push(it->item());
                    }
                    delete it;
                } else
                    stack.push(obj);
                pc++;
                break;
            }
            case PACK:
            {
                CList *lst = NEWLIST;
                for (int i = stack.getSp() - 1; i >= bp + fn->getArgs()->size(); i--)
                    lst->insertFront(stack.pop());
                stack.push(lst);
                pc++;
				break;
            }
			case THIS:
			{
				stack.push(fn);
				pc++;
				break;
			}
            default:
                break;
        }
    }

    return stack.pop();
}

CObject *VirtualMachine::eval(CSymbol *sy)
{
    CObject *binding = environment->getBinding(sy);

    if (binding != NIL)
        return binding;

    return sy;
}

Stack VirtualMachine::getStack()
{
    return stack;
}

CObject *VirtualMachine::eval(CList *list)
{
	CallFrame base;
    UserFunction *fn = new UserFunction(NEWLIST);
	base.setEnvironment(environment);
    auto bc = Compiler::compileList(list, fn, environment);
    bc.push_back(Instruction(RETURN, 0)); //in case of syntax and not funcall
    fn->setBytecode(bc);
	base.setFunction(fn);
    return evalBytecode(&base);
}

CObject* VirtualMachine::eval(Macro* mac, CList* list)
{
	CallFrame base;
	UserFunction *fn = new UserFunction(NEWLIST);
	base.setEnvironment(environment);
	base.setFunction(fn);
	auto bc = Compiler::compileMacroList(mac, list, fn, environment);
	bc.push_back(Instruction(RETURN, 0));
	fn->setBytecode(bc);
	return evalBytecode(&base);
}

int VirtualMachine::computeAbsoluteOuterAdress(CSymbol *sym, CallFrame *frame) const
{
    auto outer = frame->getFunction()->getContext();
    int adr = -1;

    //first find outer function which has given symbol as local name
    while (outer != nullptr)
    {
        adr = outer->getLocalAdress(sym);
        if (adr >= 0)
            break;
        outer = outer->getContext();
    }
    //now we should have proper outer function and local adress, if not its wrong
    //So we will iterate callstack upward, until we find frame with outer function and then compute absolute adr
    if (!outer || adr < 0)
        return -1; //TODO: throw exception
    auto cf = frame->getParent();
    while (cf != nullptr)
    {
        if (cf->getFunction() == outer)
            break;
        cf = cf->getParent();
    }

    return cf->getBp() + adr;
}

UserFunction *VirtualMachine::makeClosure(UserFunction *u, CallFrame *frame)
{
    CallFrame dummy(frame);
    dummy.setFunction(u);
    UserFunction *closure = new UserFunction(u->getArgs());
	closure->constTable() = u->constTable();
    auto bc = u->getBytecode();
    Environment *e = frame->getEnvironment();
    Environment *newEnv = NEWENV(e);

    for (auto it = bc.begin(); it != bc.end(); ++it)
    {
        if (it->bc == STORE_OUTER)
        {
            CSymbol *symbol = memory.symbolTable().getSymbol(it->arg);
            if (u->getContext()->getLocalAdress(symbol) >= 0)
            {
                it->bc = STORE;
            }
        }
        if (it->bc == LOAD_OUTER)
        {
            CSymbol *symbol = memory.symbolTable().getSymbol(it->arg);
            if (u->getContext()->getLocalAdress(symbol) >= 0)
            {
                int adr = computeAbsoluteOuterAdress(symbol, &dummy);
                newEnv->bind(symbol, stack[adr]);
                it->bc = LOAD;
            }
        }

    }
    closure->setBytecode(bc);
    if (newEnv->size() > 0)
        closure->setEnvironment(newEnv);
    if (u->getContext())
        closure->setContext(u->getContext()->getContext());

    return closure;
}

void VirtualMachine::initTopEnvironment()
{
    Environment *top = Environment::top();
    top->bind(NEWSYMBOL("true"), TRUE);
    top->bind(NEWSYMBOL("false"), FALSE);
    top->bind(NEWSYMBOL("nil"), NIL);
    top->bind(NEWSYMBOL("stdin"), STDIN);
    top->bind(NEWSYMBOL("stdout"), STDOUT);

    top->bind(NEWSYMBOL("+"), new BuiltinPlus);
    top->bind(NEWSYMBOL("-"), new BuiltinMinus);
    top->bind(NEWSYMBOL("*"), new BuiltinTimes);
    top->bind(NEWSYMBOL("/"), new BuiltinDivide);

    top->bind(NEWSYMBOL("quit"), new BuiltinQuit(this));

    top->bind(NEWSYMBOL("set"), new SetSyntax);
    top->bind(NEWSYMBOL("if"), new IfSyntax);

    top->bind(NEWSYMBOL("eq"), new BuiltinBaseEquals);
    top->bind(NEWSYMBOL(">"), new BuiltinGreaterThan);
    top->bind(NEWSYMBOL("<"), new BuiltinSmallerThan);

    top->bind(NEWSYMBOL("and"), new BuiltinAnd);
    top->bind(NEWSYMBOL("or"), new BuiltinOr);
    top->bind(NEWSYMBOL("not"), new BuiltinNot);

    top->bind(NEWSYMBOL("lambda"), new Lambda);
    top->bind(NEWSYMBOL("quote"), new QuoteSyntax);
    top->bind(NEWSYMBOL("do"), new DoSyntax);
    top->bind(NEWSYMBOL("defn"), new DefnSyntax);
    top->bind(NEWSYMBOL("@"), new UnpackSyntax);
	top->bind(NEWSYMBOL("`"), new BackquoteSyntax);
	top->bind(NEWSYMBOL(","), new UnquoteSyntax);
	top->bind(NEWSYMBOL("defmacro"), new DefMacroSyntax(this));
	top->bind(NEWSYMBOL("this"), new ThisSyntax);

    top->bind(NEWSYMBOL("list"), new BuiltinList);
    top->bind(NEWSYMBOL("size"), new BuiltinSize);
    top->bind(NEWSYMBOL("first"), new BuiltinFirst);
    top->bind(NEWSYMBOL("last"), new BuiltinLast);
    top->bind(NEWSYMBOL("rest"), new BuiltinRest);
    top->bind(NEWSYMBOL("insertFront"), new BuiltinInsertFront);
    top->bind(NEWSYMBOL("insertBack"), new BuiltinInsertBack);
    top->bind(NEWSYMBOL("seq"), new BuiltinSeq);
    top->bind(NEWSYMBOL("clone"), new BuiltinClone);
    top->bind(NEWSYMBOL("make-map"), new BuiltinMap);
    top->bind(NEWSYMBOL("concat"), new BuiltinConcat);
    top->bind(NEWSYMBOL("at"), new BuiltinAt);
    top->bind(NEWSYMBOL("setf"), new BuiltinSetf);


    top->bind(NEWSYMBOL("print"), new BuiltinPrint);
    top->bind(NEWSYMBOL("open"), new BuiltinOpen);
    top->bind(NEWSYMBOL("read"), new BuiltinRead);
    top->bind(NEWSYMBOL("readline"), new BuiltinReadLine);
    top->bind(NEWSYMBOL("readObject"), new BuiltinReadObject);
    top->bind(NEWSYMBOL("close"), new BuiltinClose);
    top->bind(NEWSYMBOL("load"), new BuiltinLoad(this));
}

void VirtualMachine::includeStdLib()
{
    std::filebuf fb;
    if (fb.open("stdlib.cl", std::ios::in))
    {
        std::istream is(&fb);
        Lexer lexer(is);
        Parser parser(lexer);
        while (true)
        {
            CObject *exp = parser.parse();
            if (exp != nullptr)
            {
                CObject *result = exp->eval(this);
            }
            parser.getLexer().readNext();
            if (parser.getLexer().getToken().getType() == Token::END)
            {
                break;
            }
        }

        fb.close();
    }
}