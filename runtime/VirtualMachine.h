#pragma once
#include "../io/Reader.h"
#include "IEvaluator.h"
#include "Environment.h"
#include "Stack.h"
#include "CallFrame.h"
#include "../runtime/IStackGetter.h"

class UserFunction;

class VirtualMachine :
	public IEvaluator, IStackGetter
{
public:
	VirtualMachine();
	virtual ~VirtualMachine();
	void run();
	void stop();
	CObject* evalBytecode(CallFrame* frame);

	//IEvaluator interface
	CObject* eval(CSymbol*) override;
	CObject* eval(CList*) override;
	CObject* eval(Macro *, CList*) override;

  virtual Stack getStack() override;

private:
	int computeAbsoluteOuterAdress(CSymbol * sym, CallFrame* frame) const;
	UserFunction * makeClosure(UserFunction *u, CallFrame *frame);
	void initTopEnvironment();
    void includeStdLib();

	Reader reader;
	Environment * environment;
    Stack stack;
	bool running;
};

