(set satProblem (list))
(set variableCount 0)
(set clauseCount 0)
(set solution (list))
(set lastTested (list))

(defn readClause (currentClause fs)
	(set num (readObject fs))
	(if (eq num 0)
		()
		(
			do
			(insertBack (at satProblem currentClause) num)
			(readClause currentClause fs)
		)
	)
)

(defn readSatRecursive (index fs)
	(if (eq index 0)
		()
		(
			do
			(insertBack satProblem (list))
			(readClause (- clauseCount index) fs)
			(set index (- index 1))
			(readSatRecursive index fs)
		)
	)
)

(defn readSat (fs)
	(set variableCount (readObject fs))
	(set clauseCount (readObject fs))
	(readSatRecursive clauseCount fs)
)


(defn initSolution (position)
	(if (eq position 0)
		()
		(
			do
			(insertBack solution false)
			(set position (- position 1))
			(initSolution position)
		)
	)
)

(defn initLastTestedSolution (position)
	(if (eq position 0)
		()
		(
			do
			(insertBack lastTested true)
			(set position (- position 1))
			(initLastTestedSolution position)
		)
	)
)




(defn nextSolution (index)
	(if (eq (at solution index) false)
		(setf solution index true)
		(
			do
			(setf solution index false)
			(set index (- index 1))
			(nextSolution index)
		)
	)
)

(defn isClauseTrue (currentClause index)
	(set clause (at satProblem currentClause) )
	(set currentVariable (at clause index) )
	(set currentValue (at solution (- currentVariable 1)))
	(if (< currentVariable < 0)
		(
			do
			(set absValue (+ currentValue (* currentValue 2)))
			(set currentValue (at solution (- absValue 1)))
		)
		()
	)
	
	(if (eq index -1)
		false
		(if (or (and (> currentVariable 0) (eq currentValue true)) (and (< currentVariable 0) (eq currentValue false)))
			true
			(
				do
				(set index (- index 1))
				(isClauseTrue currentClause index)
			)
		)
	)
)

(defn satCheck (currentClause)
	(if (eq currentClause -1)
		true
		(
			do
			(set clause (at satProblem currentClause) )
			(set clauseSize (size clause))
			(if (isClauseTrue currentClause (- clauseSize 1)) 
				(
					do
					(set currentClause (- currentClause 1))
					(satCheck currentClause)
				)
				false
			)
		)
	)
)

(defn generateSolutions (index)
	(if (compareLists solution lastTested variableCount)
		()
		(
			do
			(if (satCheck (- clauseCount 1))
				(print "correct: " solution "\n")
				(print "wrong: " solution "\n")
			)
			(set index (- index 1))
			(set solution (nextSolution (- variableCount 1)))
			(generateSolutions index)
		)
	)
)

(defn lastSolCheck()
	(if (satCheck (- clauseCount 1))
		(print "correct: " solution "\n")
		(print "wrong: " solution "\n")
	)
)

(defn loadSat (path) 
	(set fs (open path))
	(readSat fs)
	(close fs)
	(initSolution variableCount)
	(initLastTestedSolution variableCount)
	(print "sat problem loaded: " satProblem "\n")
)

(defn solveSat () 
	(generateSolutions clauseCount)
	(lastSolCheck)
)