(defn reverse (lst)
	(if (eq (size lst) 0)
		(list)
		(insertBack (reverse (rest lst)) (first lst))
	)
)

(defn select (pred lst) (iselect pred lst (list)))

(defn iselect (pred lst result)
	(if (eq (size lst) 0)
		result
		(if (pred (first lst))
			(iselect pred (rest lst) (insertBack result (first lst)))
			(iselect pred (rest lst) result)
		)
	)
)

(defn map (fn lst) (imap fn lst (list)))	

(defn imap (fn lst result)
	(if (eq (size lst) 0)
		result
		(imap fn (rest lst) (insertBack result (fn (first lst))))
	)
)

(defn reduce (fn lst) (ireduce fn (rest lst) (first lst)))	

(defn ireduce (fn lst result)
	(if (eq (size lst) 0)
		result
		(ireduce fn (rest lst) (fn result (first lst)))
	)
)

(defn compareLists (listA listB index)
	(if (eq index -1) 
		true
		(if (eq (at listA index) (at listB index) )
			(
				do
				(set index (- index 1))
				(compareLists listA listB index)
			)
			false
		)
	)
)


(defn factorial (a) (if (eq a 1) 1 (* a (factorial (- a 1)))))

(defn crest (lst) (clone (rest lst)))