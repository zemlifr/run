#pragma once

#include <exception>

class ParseError : public std::exception
{
public:
	ParseError(std::string msg) : exception(msg.c_str()){};
};

class RuntimeError : public std::exception
{
public:
	RuntimeError(std::string msg) : exception(msg.c_str()) {};
};

class OutOfRangeError : public std::exception
{
public:
  OutOfRangeError(std::string msg) : exception() {};
};