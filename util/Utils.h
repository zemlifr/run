#pragma once

#include <functional>
#include <string>

#include "../lang/CObject.h"
#include "../lang/Atoms.h"

struct AtomHash
{
	std::size_t operator()(CObject* k) const
	{
		using std::size_t;
		using std::hash;

		switch (k->getType())
		{
		case SYMBOL:
			return hash<std::string>()(((CSymbol*)k)->getVal());
		case STRING:
			return hash<std::string>()(((CString*)k)->getVal());
		case LONG:
			return hash<long long>()(((CLong*)k)->getVal());
		case FLOAT:
			return hash<double>()(((CFloat*)k)->getVal());
		case BOOL:
			return hash<bool>()(((CLong*)k)->getVal());
		default:
			return 0; //exception for nonatomic types
		}
	}
};

struct ObjectComparator
{
	bool operator()(CObject* a, CObject * b) const
	{
		if (a->getType() != b->getType())
			return false;
		switch (a->getType())
		{
		case SYMBOL:
			return a == b;
		case STRING:
			return ((CString*)a)->getVal() == ((CString*)b)->getVal();
		case LONG:
			return ((CLong*)a)->getVal() == ((CLong*)b)->getVal();
		case FLOAT:
			return ((CFloat*)a)->getVal() == ((CFloat*)b)->getVal();
		case BOOL:
			return ((CFloat*)a)->getVal() == ((CFloat*)b)->getVal();
		default:
			return a == b;
		}

	}
};